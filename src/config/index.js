export const HostAPI = "https://mytutor-admin-bk.herokuapp.com";
// export const HostAPI = "http://localhost:3000";
export const limitItem = 6;
export const ErrorHandler = function ErrorHandler(error) {
  let errorMessage = "Đã xảy ra lỗi";
  if (error === "old.pass.incorrect") {
    errorMessage = "Mật khẩu cũ không chính xác";
  } else if (error === "user.notfound") {
    errorMessage = "Người dùng không tồn tại";
  } else if (error === "failed") {
    errorMessage = "Lỗi hệ thống";
  } else if (error === "user.already.exist") {
    errorMessage = "Người dùng đã tồn tại";
  } else if (error === "user.locked") {
    errorMessage =
      "Tài khoản của bạn đã bị khóa. Vui lòng liên hệ quản trị viên.";
  } else if (error === "user.incorrect") {
    errorMessage = "Thông tin đăng nhập không chính xác";
  }
  return errorMessage;
};
