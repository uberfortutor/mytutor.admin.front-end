import React from "react";
import { connect } from "react-redux";
import ComplainDetailComponent from "../../../component/complain/dialog/ComplainDetailComponent";
import {
  requestGetComplainDetailRefresh,
  getComplainDetail
} from "../../../action/complain";

const mapStateToProps = state => ({
  ...state.RequestReducer
});

class ComplainDetailContainer extends React.Component {
  componentDidUpdate() {
    const { isRequest, show, idComplain, data, dispatch } = this.props;
    if (!data && show && !isRequest) {
      dispatch(getComplainDetail(idComplain));
    }
  }

  handleCloseDialog() {
    const { handleClose, dispatch } = this.props;
    dispatch(requestGetComplainDetailRefresh());
    handleClose();
  }

  render() {
    const { error, show, isRequest, data } = this.props;
    return (
      <ComplainDetailComponent
        show={show}
        error={error}
        handleClose={() => this.handleCloseDialog()}
        isRequest={isRequest}
        complain={data}
      />
    );
  }
}

export default connect(mapStateToProps)(ComplainDetailContainer);
