import React from "react";
import { connect } from "react-redux";
import { limitItem } from "../../config";
import ComplainComponent from "../../component/complain/ComplainComponent";
import TitleComponent from "../../component/TitleComponent";
import LoadingComponent from "../../component/loading/LoadingComponent";
import {
  getListComplain,
  changeStatusComplain,
  updateComplainStatus,
  requestUpdateComplainRefresh,
  clearDataComplain
} from "../../action/complain";
import ComplainDetailContainer from "./dialog/ComplainDetailContainer";

const mapStateToProps = state => ({
  ...state.ComplainReducer,
  isRefresh: state.RequestReducer.isRefresh
});

class ComplainContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentPage: 1,
      status: -1,
      showDetailComplain: false,
      idComplain: null,
      searchText: ""
    };
  }

  componentDidMount() {
    const { currentPage, status } = this.state;
    this.refreshData(currentPage, status);
  }

  componentDidUpdate() {
    const { isRequest, isRefresh, dispatch } = this.props;
    const { currentPage, status, searchText } = this.state;
    if (!isRequest && isRefresh) {
      dispatch(requestUpdateComplainRefresh());
      this.refreshData(currentPage, status, searchText);
    }
  }

  componentWillUnmount() {
    const { dispatch } = this.props;
    dispatch(clearDataComplain());
  }

  onChangeStatus(e, id) {
    e.stopPropagation();
    const { dispatch, isRequest } = this.props;
    if (!isRequest) {
      dispatch(changeStatusComplain(id));
      dispatch(updateComplainStatus(id));
    }
  }

  onNext() {
    const { currentPage, status, searchText } = this.state;
    this.setState({
      currentPage: currentPage + 1
    });
    this.refreshData(currentPage + 1, status, searchText);
  }

  onPrev() {
    const { currentPage, status, searchText } = this.state;
    this.setState({
      currentPage: currentPage - 1
    });
    this.refreshData(currentPage - 1, status, searchText);
  }

  onFilter(status) {
    const { searchText } = this.state;
    this.setState({
      currentPage: 1,
      status
    });
    this.refreshData(1, status, searchText);
  }

  onItemClick(idComplain) {
    this.setState({
      showDetailComplain: true,
      idComplain
    });
  }

  onSearchChange(e) {
    this.setState({
      searchText: e.target.value
    });
  }

  onSearch() {
    const { status, searchText } = this.state;
    this.setState({
      currentPage: 1
    });
    this.refreshData(1, status, searchText);
  }

  handleClose() {
    this.setState({
      showDetailComplain: false
    });
  }

  refreshData(page, status, searchText) {
    const { dispatch, isRequest } = this.props;
    if (!isRequest) {
      if (page === undefined) {
        dispatch(getListComplain(1, status, searchText));
      } else {
        dispatch(getListComplain(page, status, searchText));
      }
    }
  }

  render() {
    const { listComplain, totalComplain, isRequest } = this.props;
    const {
      currentPage,
      status,
      showDetailComplain,
      idComplain,
      searchText
    } = this.state;

    if (listComplain) {
      return (
        <>
          <ComplainDetailContainer
            show={showDetailComplain}
            handleClose={() => this.handleClose()}
            idComplain={idComplain}
          />
          <TitleComponent />
          <ComplainComponent
            searchText={searchText}
            onSearch={() => this.onSearch()}
            onSearchChange={e => this.onSearchChange(e)}
            onItemClick={idC => this.onItemClick(idC)}
            isRequest={isRequest}
            listComplain={listComplain}
            onChangeStatus={(e, id) => this.onChangeStatus(e, id)}
            onPrev={() => this.onPrev()}
            onNext={() => this.onNext()}
            onFilter={st => this.onFilter(st)}
            currentPage={currentPage}
            status={status}
            maxPage={
              Math.floor(totalComplain / limitItem) +
              (totalComplain % limitItem === 0 ? 0 : 1)
            }
          />
        </>
      );
    }
    return <LoadingComponent />;
  }
}

export default connect(mapStateToProps)(ComplainContainer);
