import React from "react";
import { connect } from "react-redux";
import AmountComponent from "../../component/benefit/AmountComponent";
import LoadingComponent from "../../component/loading/LoadingComponent";
import { getListTopUser, clearDataTopUser } from "../../action/benefit";

const mapStateToProps = state => ({
  ...state.AmountReducer
});

class AmountContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      dateBegin: new Date()
        .toJSON()
        .slice(0, 10)
        .replace(/-/g, "/"),
      dateEnd: new Date()
        .toJSON()
        .slice(0, 10)
        .replace(/-/g, "/")
    };
  }

  componentDidMount() {
    const { dateBegin, dateEnd } = this.state;
    this.refreshData(dateBegin, dateEnd);
  }

  componentDidUpdate() {
    const { isRequestListUser, isRefresh } = this.props;
    if (!isRequestListUser && isRefresh) {
      const { dateBegin, dateEnd } = this.state;
      this.refreshData(dateBegin, dateEnd);
    }
  }

  componentWillUnmount() {
    const { dispatch } = this.props;
    dispatch(clearDataTopUser());
  }

  refreshData(dateBegin, dateEnd) {
    const { dispatch, isRequest } = this.props;
    if (!isRequest) {
      dispatch(getListTopUser(dateBegin, dateEnd, 3));
    }
  }

  render() {
    const { listBenefit } = this.props;
    if (listBenefit) {
      return <AmountComponent listBenefit={listBenefit} />;
    }
    return <LoadingComponent />;
  }
}

export default connect(mapStateToProps)(AmountContainer);
