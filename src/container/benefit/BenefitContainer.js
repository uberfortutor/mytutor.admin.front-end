import React from "react";
import { connect } from "react-redux";
import BenefitComponent from "../../component/benefit/BenefitComponent";
import { clearDataBenefit, changeBeginDate } from "../../action/benefit";

const mapStateToProps = state => ({
  ...state.BenefitReducer
});

class BenefitContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      type: 0,
      filter: 0
    };
  }

  componentWillUnmount() {
    const { dispatch } = this.props;
    dispatch(clearDataBenefit());
  }

  onFilter(e) {
    const { filter } = this.state;
    const { dispatch } = this.props;
    // eslint-disable-next-line radix
    const value = parseInt(e.target.value);
    if (filter !== value) {
      this.setState({
        filter: value
      });

      let beginDate = new Date();
      if (value === 1) {
        if (beginDate.getDay() > 0) {
          beginDate.setDate(beginDate.getDate() - beginDate.getDay() + 1);
        } else {
          beginDate.setDate(beginDate.getDate() - 6);
        }
      } else if (value === 2) {
        beginDate.setDate(beginDate.getDate() - 30);
      } else if (value === 3) {
        beginDate.setDate(beginDate.getDate() - 90);
      } else if (value === 4) {
        beginDate = undefined;
      }
      if (beginDate !== undefined) {
        beginDate = `${beginDate.getFullYear()}/${beginDate.getMonth() +
          1}/${beginDate.getDate()}`;
      }
      dispatch(changeBeginDate(beginDate));
    }
  }

  onFilterType(e) {
    const { type } = this.state;
    // eslint-disable-next-line radix
    const value = parseInt(e.target.value);
    if (type !== value) {
      this.setState({
        type: value
      });
    }
  }

  render() {
    const { type, filter } = this.state;
    const filterItems = [
      {
        value: 0,
        title: "Hôm nay"
      },
      {
        value: 1,
        title: "Tuần này"
      },
      {
        value: 2,
        title: "30 ngày"
      },
      {
        value: 3,
        title: "90 ngày"
      },
      {
        value: 4,
        title: "Tất cả"
      }
    ];

    const typeItems = [
      {
        value: 0,
        title: "Người dùng"
      },
      {
        value: 1,
        title: "Kỹ năng"
      }
    ];

    return (
      <BenefitComponent
        type={type}
        filter={filter}
        filterItems={filterItems}
        typeItems={typeItems}
        onFilter={e => this.onFilter(e)}
        onFilterType={e => this.onFilterType(e)}
      />
    );
  }
}

export default connect(mapStateToProps)(BenefitContainer);
