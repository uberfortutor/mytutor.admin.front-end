import React from "react";
import { connect } from "react-redux";
import LoadingComponent from "../../component/loading/LoadingComponent";
import { getListTopSkill, clearDataTopSkill } from "../../action/benefit";
import { limitItem } from "../../config";
import TopSkillComponent from "../../component/benefit/TopSkillComponent";
import TitleComponent from "../../component/TitleComponent";

const mapStateToProps = state => ({
  ...state.TopSkillReducer,
  dateBegin: state.BenefitReducer.dateBegin
});

class TopSkillContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentPage: 1,
      date: -1
    };
  }

  componentDidMount() {
    const { currentPage } = this.state;
    const { dateBegin } = this.props;
    this.setState({
      // eslint-disable-next-line react/no-access-state-in-setstate
      ...this.state,
      date: dateBegin
    });
    this.refreshData(dateBegin, currentPage);
  }

  componentDidUpdate() {
    const { isRequestListUser, dateBegin } = this.props;
    const { date } = this.state;
    if (!isRequestListUser && dateBegin !== date) {
      // eslint-disable-next-line react/no-did-update-set-state
      this.setState({
        // eslint-disable-next-line react/no-access-state-in-setstate
        ...this.state,
        date: dateBegin,
        currentPage: 1
      });
      this.refreshData(dateBegin, 1);
    }
  }

  componentWillUnmount() {
    const { dispatch } = this.props;
    dispatch(clearDataTopSkill());
  }

  onNext() {
    const { dateBegin } = this.props;
    const { currentPage } = this.state;
    this.setState({
      currentPage: currentPage + 1
    });
    this.refreshData(dateBegin, currentPage + 1);
  }

  onPrev() {
    const { dateBegin } = this.props;
    const { currentPage } = this.state;
    this.setState({
      currentPage: currentPage - 1
    });
    this.refreshData(dateBegin, currentPage - 1);
  }

  refreshData(dateBegin, page) {
    const { dispatch, isRequest } = this.props;
    if (!isRequest) {
      dispatch(getListTopSkill(dateBegin, page));
    }
  }

  render() {
    const { listSkill, isRequest, totalSkill } = this.props;
    const { currentPage } = this.state;

    if (listSkill) {
      return (
        <>
          <TitleComponent />
          <TopSkillComponent
            onNext={() => this.onNext()}
            onPrev={() => this.onPrev()}
            listSkill={listSkill}
            isRequest={isRequest}
            currentPage={currentPage}
            maxPage={
              Math.floor(totalSkill / limitItem) +
              (totalSkill % limitItem === 0 ? 0 : 1)
            }
          />
        </>
      );
    }
    return <LoadingComponent />;
  }
}

export default connect(mapStateToProps)(TopSkillContainer);
