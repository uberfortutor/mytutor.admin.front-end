import React from "react";
import { connect } from "react-redux";
import ContractDetailComponent from "../../../component/contract/dialog/ContractDetailComponent";

class ContractDetailContainer extends React.Component {
  handleCloseDialog() {
    const { handleClose } = this.props;
    handleClose();
  }

  render() {
    const { show, contract } = this.props;
    return (
      <ContractDetailComponent
        show={show}
        handleClose={() => this.handleCloseDialog()}
        contract={contract}
      />
    );
  }
}

export default connect()(ContractDetailContainer);
