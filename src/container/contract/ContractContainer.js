import React from "react";
import { connect } from "react-redux";
import { limitItem } from "../../config";
import TitleComponent from "../../component/TitleComponent";
import LoadingComponent from "../../component/loading/LoadingComponent";
import {
  getListContract,
  clearDataContract,
  changeStatusContract,
  updateContractStatus,
  requestUpdateContractRefresh
} from "../../action/contract";
import ContractDetailContainer from "./dialog/ContractDetailContainer";
import ContractComponent from "../../component/contract/ContractComponent";

const mapStateToProps = state => ({
  ...state.ContractReducer,
  isRefresh: state.RequestReducer.isRefresh
});

class ContractContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentPage: 1,
      status: -1,
      showDetailContract: false,
      contract: null,
      searchText: ""
    };
  }

  componentDidMount() {
    const { currentPage, status } = this.state;
    this.refreshData(currentPage, status);
  }

  componentDidUpdate() {
    const { isRequest, isRefresh, dispatch } = this.props;
    const { currentPage, status, searchText } = this.state;
    if (!isRequest && isRefresh) {
      dispatch(requestUpdateContractRefresh());
      this.refreshData(currentPage, status, searchText);
    }
  }

  componentWillUnmount() {
    const { dispatch } = this.props;
    dispatch(clearDataContract());
  }

  onChangeStatus(e, id) {
    e.stopPropagation();
    const { dispatch, isRequest } = this.props;
    if (!isRequest) {
      dispatch(changeStatusContract(id));
      dispatch(updateContractStatus(id));
    }
  }

  onNext() {
    const { currentPage, status, searchText } = this.state;
    this.setState({
      currentPage: currentPage + 1
    });
    this.refreshData(currentPage + 1, status, searchText);
  }

  onPrev() {
    const { currentPage, status, searchText } = this.state;
    this.setState({
      currentPage: currentPage - 1
    });
    this.refreshData(currentPage - 1, status, searchText);
  }

  onFilter(status) {
    const { searchText } = this.state;
    this.setState({
      currentPage: 1,
      status
    });
    this.refreshData(1, status, searchText);
  }

  onItemClick(contract) {
    this.setState({
      showDetailContract: true,
      contract
    });
  }

  onSearchChange(e) {
    this.setState({
      searchText: e.target.value
    });
  }

  onSearch() {
    const { status, searchText } = this.state;
    this.setState({
      currentPage: 1
    });
    this.refreshData(1, status, searchText);
  }

  handleClose() {
    this.setState({
      showDetailContract: false
    });
  }

  refreshData(page, status, searchText) {
    const { dispatch, isRequest } = this.props;
    if (!isRequest) {
      if (page === undefined) {
        dispatch(getListContract(1, status, searchText));
      } else {
        dispatch(getListContract(page, status, searchText));
      }
    }
  }

  render() {
    const { listContract, totalContract, isRequest } = this.props;
    const {
      currentPage,
      status,
      showDetailContract,
      contract,
      searchText
    } = this.state;

    if (listContract) {
      return (
        <>
          <ContractDetailContainer
            show={showDetailContract}
            handleClose={() => this.handleClose()}
            contract={contract}
          />
          <TitleComponent />
          <ContractComponent
            searchText={searchText}
            onSearch={() => this.onSearch()}
            onSearchChange={e => this.onSearchChange(e)}
            onItemClick={idC => this.onItemClick(idC)}
            isRequest={isRequest}
            listContract={listContract}
            onChangeStatus={(e, id) => this.onChangeStatus(e, id)}
            onPrev={() => this.onPrev()}
            onNext={() => this.onNext()}
            onFilter={st => this.onFilter(st)}
            currentPage={currentPage}
            status={status}
            maxPage={
              Math.floor(totalContract / limitItem) +
              (totalContract % limitItem === 0 ? 0 : 1)
            }
          />
        </>
      );
    }
    return <LoadingComponent />;
  }
}

export default connect(mapStateToProps)(ContractContainer);
