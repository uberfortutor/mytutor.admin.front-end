import React from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import HomeComponent from "../../component/home/HomeComponent";
import { ping } from "../../action/home";

const mapStateToProps = state => ({
  ...state.HomeReducer
});

class HomeContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      intervalPing: null
    };
  }

  componentDidMount() {
    const { dispatch, isRequest, error } = this.props;
    if (!isRequest && !error) {
      dispatch(ping());
    }
    const { intervalPing } = this.state;
    if (intervalPing == null) {
      this.setState({
        intervalPing: setInterval(() => {
          if (!isRequest && !error) {
            dispatch(ping());
          }
        }, 1000 * 30)
      });
    }
  }

  componentWillUnmount() {
    const { intervalPing } = this.state;
    if (intervalPing == null) {
      clearInterval(intervalPing);
    }
  }

  render() {
    const { path, error, role } = this.props;
    const token = localStorage.getItem("userToken");
    // redirect to home page
    if (token === null || token === "" || token === undefined || error) {
      return <Redirect to="/login" />;
    }
    return <HomeComponent path={path} role={role} />;
  }
}

export default connect(mapStateToProps)(HomeContainer);
