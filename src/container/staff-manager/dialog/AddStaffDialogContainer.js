import React from "react";
import { connect } from "react-redux";
import AddStaffDialogComponent from "../../../component/staff-manager/dialog/AddStaffDialogComponent";
import { addStaff, requestAddStaffError } from "../../../action/staff-manager";

const mapStateToProps = state => ({
  ...state.RequestReducer
});

class AddStaffDialogContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      email: "",
      name: "",
      role: 1
    };
  }

  componentDidUpdate() {
    const { handleClose, requestDone, show } = this.props;
    if (requestDone && show) {
      handleClose();
    }
  }

  handleAddClick(e) {
    e.preventDefault();
    const { dispatch, isRequest } = this.props;
    const { state } = this;
    if (
      state.name === "" ||
      state.password === "" ||
      state.username === "" ||
      // eslint-disable-next-line no-restricted-globals
      isNaN(state.role) ||
      state.email === ""
    ) {
      dispatch(requestAddStaffError("Vui lòng diền đầy đủ thông tin"));
    } else if (!isRequest) {
      dispatch(addStaff(state));
    }
  }

  handleUsernameChange(e) {
    const { state } = this;
    this.setState({
      ...state,
      username: e.target.value
    });
  }

  handleNameChange(e) {
    const { state } = this;
    this.setState({
      ...state,
      name: e.target.value
    });
  }

  handlePasswordChange(e) {
    const { state } = this;
    this.setState({
      ...state,
      password: e.target.value
    });
  }

  handleRoleChange(e) {
    const { state } = this;
    this.setState({
      ...state,
      role: e.target.value
    });
  }

  handleEmailChange(e) {
    const { state } = this;
    this.setState({
      ...state,
      email: e.target.value
    });
  }

  render() {
    const { error, show, handleClose, isRequest } = this.props;
    return (
      <AddStaffDialogComponent
        show={show}
        error={error}
        handleClose={handleClose}
        isRequest={isRequest}
        handleAdd={e => this.handleAddClick(e)}
        handleUsernameChange={e => this.handleUsernameChange(e)}
        handleNameChange={e => this.handleNameChange(e)}
        handlePasswordChange={e => this.handlePasswordChange(e)}
        handleRoleChange={e => this.handleRoleChange(e)}
        handleEmailChange={e => this.handleEmailChange(e)}
      />
    );
  }
}

export default connect(mapStateToProps)(AddStaffDialogContainer);
