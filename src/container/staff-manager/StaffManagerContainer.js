import React from "react";
import { connect } from "react-redux";
import StaffManagerComponent from "../../component/staff-manager/StaffManagerComponent";
import {
  getListStaff,
  requestAddStaffRefresh,
  clearDataStaff,
  changeStatusStaff,
  changeStatus,
  resetPassStaffStatus,
  resetPasswordStaff
} from "../../action/staff-manager";
import { limitItem } from "../../config";
import LoadingComponent from "../../component/loading/LoadingComponent";
import TitleComponent from "../../component/TitleComponent";

const mapStateToProps = state => ({
  ...state.StaffManagerReducer
});

class StaffManagerContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentPage: 1,
      showAddDialog: false,
      roleFilter: -1
    };
  }

  componentDidMount() {
    const { currentPage, roleFilter } = this.state;
    this.refreshData(currentPage, roleFilter);
  }

  componentDidUpdate() {
    const { isRequestListStaff, isRefresh } = this.props;
    const { currentPage, roleFilter } = this.state;
    if (!isRequestListStaff && isRefresh) {
      this.refreshData(currentPage, roleFilter);
    }
  }

  componentWillUnmount() {
    const { dispatch } = this.props;
    dispatch(clearDataStaff());
  }

  onNext() {
    const { currentPage, roleFilter } = this.state;
    this.setState({
      currentPage: currentPage + 1
    });
    this.refreshData(currentPage + 1, roleFilter);
  }

  onPrev() {
    const { currentPage, roleFilter } = this.state;
    this.setState({
      currentPage: currentPage - 1
    });
    this.refreshData(currentPage - 1, roleFilter);
  }

  onFilter(roleFilter) {
    this.setState({
      currentPage: 1,
      roleFilter
    });
    this.refreshData(1, roleFilter);
  }

  onChangeStatus(e, staffId, status) {
    const { dispatch } = this.props;
    dispatch(changeStatusStaff(staffId));
    dispatch(changeStatus(staffId, status));
  }

  onResetPassword(e, staffId) {
    const { dispatch } = this.props;
    dispatch(resetPassStaffStatus(staffId));
    dispatch(resetPasswordStaff(staffId));
  }

  refreshData(page, roleFilter) {
    const { dispatch, isRequestListStaff } = this.props;
    if (!isRequestListStaff) {
      if (page === undefined) {
        dispatch(getListStaff(1, roleFilter));
      } else {
        dispatch(getListStaff(page, roleFilter));
      }
    }
  }

  handleCloseAddStaffDialog() {
    const { dispatch } = this.props;
    dispatch(requestAddStaffRefresh());
    this.refreshData();
    this.setState({
      showAddDialog: false
    });
  }

  handleButtonAddClick() {
    this.setState({
      showAddDialog: true
    });
  }

  render() {
    const { listStaff, totalStaff, isRequestListStaff, role } = this.props;
    const { showAddDialog, currentPage, roleFilter } = this.state;

    if (listStaff) {
      return (
        <>
          <TitleComponent />
          <StaffManagerComponent
            onChangeStatus={(e, id, status) =>
              this.onChangeStatus(e, id, status)
            }
            onResetPassword={(e, id) => this.onResetPassword(e, id)}
            roleFilter={roleFilter}
            onFilter={rf => this.onFilter(rf)}
            role={role}
            isRequest={isRequestListStaff}
            showAddDialog={showAddDialog}
            listStaff={listStaff}
            handleCloseAddStaffDialog={() => this.handleCloseAddStaffDialog()}
            handleButtonAddClick={() => this.handleButtonAddClick()}
            onPrev={() => this.onPrev()}
            onNext={() => this.onNext()}
            totalUser={totalStaff}
            currentPage={currentPage}
            maxPage={
              Math.floor(totalStaff / limitItem) +
              (totalStaff % limitItem === 0 ? 0 : 1)
            }
          />
        </>
      );
    }
    return <LoadingComponent />;
  }
}

export default connect(mapStateToProps)(StaffManagerContainer);
