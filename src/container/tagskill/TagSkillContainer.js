import React from "react";
import { connect } from "react-redux";
import TagSkillComponent from "../../component/tagskill/TagSkillComponent";
import {
  getListTagKill,
  deleteSkill,
  requestDeleteSkillRefresh,
  requestAddSkillRefresh,
  changeStatusSkill,
  clearDataTagSkill
} from "../../action/tag-skill";
import { limitItem } from "../../config";
import LoadingComponent from "../../component/loading/LoadingComponent";
import TitleComponent from "../../component/TitleComponent";

const mapStateToProps = state => ({
  ...state.TagSkillReducer,
  isRefresh: state.RequestReducer.isRefresh
});

class TagKillContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentPage: 1,
      showAddDialog: false
    };
  }

  componentDidMount() {
    const { currentPage } = this.state;
    this.refreshData(currentPage);
  }

  componentDidUpdate() {
    const { isRefresh, isRequest, dispatch } = this.props;
    if (!isRequest && isRefresh) {
      dispatch(requestDeleteSkillRefresh());
      const { currentPage } = this.state;
      this.refreshData(currentPage);
    }
  }

  componentWillUnmount() {
    const { dispatch } = this.props;
    dispatch(clearDataTagSkill());
  }

  onNext() {
    const { currentPage } = this.state;
    this.setState({
      currentPage: currentPage + 1
    });
    this.refreshData(currentPage + 1);
  }

  onPrev() {
    const { currentPage } = this.state;
    this.setState({
      currentPage: currentPage - 1
    });
    this.refreshData(currentPage - 1);
  }

  onDelete(e, id) {
    const { dispatch, isRequest } = this.props;
    if (!isRequest) {
      dispatch(changeStatusSkill(id));
      dispatch(deleteSkill(id));
    }
  }

  handleCloseAddTagSkillDialog() {
    const { currentPage } = this.state;
    const { dispatch } = this.props;
    dispatch(requestAddSkillRefresh());
    this.refreshData(currentPage);
    this.setState({
      showAddDialog: false
    });
  }

  handleButtonAddClick() {
    this.setState({
      showAddDialog: true
    });
  }

  refreshData(page) {
    const { dispatch, isRequest } = this.props;
    if (!isRequest) {
      if (page === undefined) {
        dispatch(getListTagKill(1));
      } else {
        dispatch(getListTagKill(page));
      }
    }
  }

  render() {
    const { listTagSkill, totalSkill, isRequest } = this.props;
    const { showAddDialog, currentPage } = this.state;

    if (listTagSkill) {
      return (
        <>
          <TitleComponent />
          <TagSkillComponent
            isRequest={isRequest}
            showAddDialog={showAddDialog}
            listTagSkill={listTagSkill}
            handleCloseAddTagSkillDialog={() =>
              this.handleCloseAddTagSkillDialog()
            }
            handleButtonAddClick={() => this.handleButtonAddClick()}
            onPrev={() => this.onPrev()}
            onNext={() => this.onNext()}
            totalUser={totalSkill}
            currentPage={currentPage}
            maxPage={
              Math.floor(totalSkill / limitItem) +
              (totalSkill % limitItem === 0 ? 0 : 1)
            }
            onDelete={(e, id) => this.onDelete(e, id)}
          />
        </>
      );
    }
    return <LoadingComponent />;
  }
}

export default connect(mapStateToProps)(TagKillContainer);
