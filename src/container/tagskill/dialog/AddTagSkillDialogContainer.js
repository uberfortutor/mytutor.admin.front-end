import React from "react";
import { connect } from "react-redux";
import AddTagSkillDialogComponent from "../../../component/tagskill/dialog/AddTagSkillDialogComponent";
import { addSkill, requestAddSkillError } from "../../../action/tag-skill";

const mapStateToProps = state => ({
  ...state.RequestReducer
});

class AddStaffDialogContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      title: ""
    };
  }

  componentDidUpdate() {
    const { handleClose, requestDone, show } = this.props;
    if (requestDone && show) {
      handleClose();
    }
  }

  handleAddClick(e) {
    e.preventDefault();
    const { dispatch, isRequest } = this.props;
    const { state } = this;
    if (state.title === "") {
      dispatch(requestAddSkillError("Vui lòng diền đầy đủ thông tin"));
    } else if (!isRequest) {
      dispatch(addSkill(state));
    }
  }

  handleTitleChange(e) {
    const { state } = this;
    this.setState({
      ...state,
      title: e.target.value
    });
  }

  render() {
    const { error, show, handleClose, isRequest } = this.props;
    return (
      <AddTagSkillDialogComponent
        show={show}
        error={error}
        handleClose={handleClose}
        isRequest={isRequest}
        handleAdd={e => this.handleAddClick(e)}
        handleTitleChange={e => this.handleTitleChange(e)}
      />
    );
  }
}

export default connect(mapStateToProps)(AddStaffDialogContainer);
