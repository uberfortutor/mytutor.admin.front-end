import React from "react";
import { connect } from "react-redux";
import UserDetailComponent from "../../../component/users/dialog/UserDetailComponent";
import {
  getUserDetail,
  requestGetUserDetailRefresh
} from "../../../action/users";

const mapStateToProps = state => ({
  ...state.RequestReducer
});

class UserDetailContainer extends React.Component {
  componentDidUpdate() {
    const { isRequest, show, idUser, data, dispatch } = this.props;
    if (!data && show && !isRequest) {
      dispatch(getUserDetail(idUser));
    }
  }

  handleCloseDialog() {
    const { handleClose, dispatch } = this.props;
    dispatch(requestGetUserDetailRefresh());
    handleClose();
  }

  render() {
    const { error, show, isRequest, data } = this.props;
    return (
      <UserDetailComponent
        show={show}
        error={error}
        handleClose={() => this.handleCloseDialog()}
        isRequest={isRequest}
        userDetail={data}
      />
    );
  }
}

export default connect(mapStateToProps)(UserDetailContainer);
