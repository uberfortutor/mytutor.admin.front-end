import React from "react";
import { connect } from "react-redux";
import UserComponent from "../../component/users/UsersComponent";
import {
  getListUser,
  unlockUser,
  lockUser,
  changeStatusUser,
  clearDataUser
} from "../../action/users";
import { limitItem } from "../../config";
import LoadingComponent from "../../component/loading/LoadingComponent";
import TitleComponent from "../../component/TitleComponent";
import UserDetailContainer from "./dialog/UserDetailContainer";

const mapStateToProps = state => ({
  ...state.UserReducer
});

class UserContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentPage: 1,
      userType: "",
      showDetailUser: false,
      userDetail: null,
      searchText: ""
    };
  }

  componentDidMount() {
    const { currentPage } = this.state;
    this.refreshData(currentPage);
  }

  componentDidUpdate() {
    const { isRequestListUser, isRefresh } = this.props;
    const { currentPage, userType, searchText } = this.state;
    if (!isRequestListUser && isRefresh) {
      this.refreshData(currentPage, userType, searchText);
    }
  }

  componentWillUnmount() {
    const { dispatch } = this.props;
    dispatch(clearDataUser());
  }

  onLock(e, id) {
    e.stopPropagation();
    const { dispatch, isRequest } = this.props;
    if (!isRequest) {
      dispatch(changeStatusUser(id));
      dispatch(lockUser(id));
    }
  }

  onUnlock(e, id) {
    e.stopPropagation();
    const { dispatch, isRequest } = this.props;
    if (!isRequest) {
      dispatch(changeStatusUser(id));
      dispatch(unlockUser(id));
    }
  }

  onNext() {
    const { currentPage, userType, searchText } = this.state;
    this.setState({
      currentPage: currentPage + 1
    });
    this.refreshData(currentPage + 1, userType, searchText);
  }

  onPrev() {
    const { currentPage, userType, searchText } = this.state;
    this.setState({
      currentPage: currentPage - 1
    });
    this.refreshData(currentPage - 1, userType, searchText);
  }

  onFilter(userType) {
    const { searchText } = this.state;
    this.setState({
      currentPage: 1,
      userType
    });
    this.refreshData(1, userType, searchText);
  }

  onItemClick(user) {
    this.setState({
      showDetailUser: true,
      userDetail: user.id
    });
  }

  onSearchChange(e) {
    this.setState({
      searchText: e.target.value
    });
  }

  onSearch() {
    const { userType, searchText } = this.state;
    this.setState({
      currentPage: 1
    });
    this.refreshData(1, userType, searchText);
  }

  handleClose() {
    this.setState({
      showDetailUser: false,
      userDetail: -1
    });
  }

  refreshData(page, userType, searchText) {
    const { dispatch, isRequest } = this.props;
    if (!isRequest) {
      if (page === undefined) {
        dispatch(getListUser(1, userType, searchText));
      } else {
        dispatch(getListUser(page, userType, searchText));
      }
    }
  }

  render() {
    const { listUser, totalUser, isRequestListUser } = this.props;
    const {
      currentPage,
      userType,
      showDetailUser,
      userDetail,
      searchText
    } = this.state;

    if (listUser) {
      return (
        <>
          <TitleComponent />
          <UserDetailContainer
            show={showDetailUser}
            idUser={userDetail}
            handleClose={() => this.handleClose()}
          />
          <UserComponent
            onSearch={() => this.onSearch()}
            searchText={searchText}
            onSearchChange={e => this.onSearchChange(e)}
            userType={userType}
            listUser={listUser}
            onFilter={u => this.onFilter(u)}
            isRequest={isRequestListUser}
            onLock={(e, id) => this.onLock(e, id)}
            onUnlock={(e, id) => this.onUnlock(e, id)}
            onPrev={() => this.onPrev()}
            onNext={() => this.onNext()}
            onItemClick={user => this.onItemClick(user)}
            totalUser={totalUser}
            currentPage={currentPage}
            maxPage={
              Math.floor(totalUser / limitItem) +
              (totalUser % limitItem === 0 ? 0 : 1)
            }
          />
        </>
      );
    }
    return <LoadingComponent />;
  }
}

export default connect(mapStateToProps)(UserContainer);
