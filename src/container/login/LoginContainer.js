import React from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import LoginComponent from "../../component/login/LoginComponent";
import TitleComponent from "../../component/TitleComponent";
import {
  login,
  requestPostLoginError,
  clearDataLogin
} from "../../action/login";

const mapStateToProps = state => ({
  ...state.LoginReducer
});

class LoginContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: ""
    };
  }

  componentWillUnmount() {
    const { dispatch } = this.props;
    dispatch(clearDataLogin());
  }

  handleSubmit(e) {
    e.preventDefault();
    const { dispatch, isRequest } = this.props;
    const { username, password } = this.state;

    if (!isRequest) {
      if (username === "" || password === "") {
        dispatch(requestPostLoginError("Vui lòng điền đầy đủ thông tin"));
      } else {
        dispatch(
          login({
            username,
            password
          })
        );
      }
    }
  }

  handlePasswordChange(e) {
    const { state } = this;
    this.setState({
      ...state,
      password: e.target.value
    });
  }

  handleUsernameChange(e) {
    const { state } = this;
    this.setState({
      ...state,
      username: e.target.value
    });
  }

  render() {
    const { isRequest, error, requestDone } = this.props;
    const token = localStorage.getItem("userToken");
    // redirect to home page
    if (requestDone && token !== null) {
      return <Redirect to="/" />;
    }
    return (
      <>
        <TitleComponent />
        <LoginComponent
          handleSubmit={e => this.handleSubmit(e)}
          handlePasswordChange={e => this.handlePasswordChange(e)}
          handleUsernameChange={e => this.handleUsernameChange(e)}
          isRequest={isRequest}
          error={error}
        />
      </>
    );
  }
}

export default connect(mapStateToProps)(LoginContainer);
