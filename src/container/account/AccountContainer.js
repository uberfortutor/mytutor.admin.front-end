import React from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router";
import AccountComponent from "../../component/account/AccountComponent";
import {
  requestChangePasswordError,
  getAccountInfo,
  changePassword,
  requestChangePasswordRefresh
} from "../../action/account";
import LoadingComponent from "../../component/loading/LoadingComponent";
import TitleComponent from "../../component/TitleComponent";

const mapStateToProps = state => ({
  ...state.RequestReducer
});

class AccountContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userInfo: null,
      isRequestChangePassword: false,
      oldPass: "",
      newPass: "",
      logout: false
    };
  }

  componentDidMount() {
    const { dispatch, isRequest, data } = this.props;
    const { userInfo } = this.state;
    if (!isRequest && !data && !userInfo) {
      dispatch(getAccountInfo());
    } else if (data && !userInfo) {
      dispatch(requestChangePasswordRefresh());
      this.setState({
        userInfo: data
      });
    }
  }

  componentDidUpdate() {
    const { requestDone, data, dispatch } = this.props;
    const { userInfo } = this.state;
    if (requestDone && data && !userInfo) {
      // eslint-disable-next-line react/no-did-update-set-state
      this.setState({
        userInfo: data
      });
      dispatch(requestChangePasswordRefresh());
    }
  }

  onChangePassword() {
    const { oldPass, newPass } = this.state;
    const { dispatch } = this.props;

    if (oldPass === "" || newPass === "") {
      dispatch(requestChangePasswordError("Không để dữ liệu trống"));
    } else {
      dispatch(changePassword(oldPass, newPass));
    }
  }

  onRequestChangePassword() {
    this.setState({
      isRequestChangePassword: true
    });
  }

  onOldPasswordChange(e) {
    const { dispatch } = this.props;
    dispatch(requestChangePasswordRefresh());
    this.setState({
      oldPass: e.target.value
    });
  }

  onNewPasswordChange(e) {
    const { dispatch } = this.props;
    dispatch(requestChangePasswordRefresh());
    this.setState({
      newPass: e.target.value
    });
  }

  onChangePasswordCancel() {
    const { dispatch } = this.props;
    dispatch(requestChangePasswordRefresh());

    this.setState({
      isRequestChangePassword: false,
      oldPass: "",
      newPass: ""
    });
  }

  onLogout() {
    localStorage.clear();
    this.setState({
      logout: true
    });
  }

  render() {
    const {
      isRequestChangePassword,
      oldPass,
      newPass,
      userInfo,
      logout
    } = this.state;
    const { isRequest, error, requestDone } = this.props;

    if (logout) {
      return <Redirect to="/login" />;
    }

    if (userInfo) {
      return (
        <>
          <AccountComponent
            userInfo={userInfo}
            onChangePassword={() => this.onChangePassword()}
            onRequestChangePassword={() => this.onRequestChangePassword()}
            isRequest={isRequest}
            isRequestChangePassword={isRequestChangePassword}
            onOldPasswordChange={e => this.onOldPasswordChange(e)}
            oldPass={oldPass}
            newPass={newPass}
            onNewPasswordChange={e => this.onNewPasswordChange(e)}
            onChangePasswordCancel={() => this.onChangePasswordCancel()}
            error={error}
            requestDone={requestDone}
            onLogout={() => this.onLogout()}
          />
          <TitleComponent />
        </>
      );
    }
    return <LoadingComponent />;
  }
}

export default connect(mapStateToProps)(AccountContainer);
