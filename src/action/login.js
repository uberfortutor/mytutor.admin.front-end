import { HostAPI, ErrorHandler } from "../config";

const axios = require("axios");

export const requestPostLogin = () => ({
  type: "REQUEST_LOGIN"
});

export const requestPostLoginDone = tokenInfo => ({
  type: "REQUEST_LOGIN_DONE",
  tokenInfo
});

export const requestPostLoginError = error => ({
  type: "REQUEST_LOGIN_FAIL",
  error: ErrorHandler(error)
});

export const clearDataLogin = () => ({
  type: "CLEAR_DATA_LOGIN"
});

export function login(user) {
  return dispatch => {
    dispatch(requestPostLogin());
    const config = {
      headers: {
        "content-type": "application/json",
        "Access-Control-Allow-Origin": "*"
      }
    };
    axios.post(`${HostAPI}/authenticate/login`, user, config).then(
      response => {
        if (response.status !== 200) {
          if (response.status !== 204) {
            return dispatch(requestPostLoginError(response.data));
          }
          return dispatch(requestPostLogin());
        }
        return dispatch(requestPostLoginDone(response.data));
      },
      err => {
        if (err.response && err.response.data) {
          dispatch(requestPostLoginError(err.response.data.message));
        } else {
          dispatch(requestPostLoginError("user.incorrect"));
        }
      }
    );
  };
}
