import { HostAPI, ErrorHandler, limitItem } from "../config";

const axios = require("axios");

export const changeBeginDate = dateBegin => ({
  type: "CHANGE_BEGIN_DATE",
  dateBegin
});

export const clearDataBenefit = () => ({
  type: "CLEAR_BEGIN_DATE"
});

export const requestGetListTopUser = () => ({
  type: "REQUEST_TOP_LIST"
});

export const requestGetListTopUserDone = listUser => ({
  type: "REQUEST_TOP_LIST_DONE",
  listUser
});

export const requestGetListTopUserError = error => ({
  type: "REQUEST_TOP_LIST_FAIL",
  error: ErrorHandler(error)
});

export const clearDataTopUser = () => ({
  type: "CLEAR_DATA_TOP_USER"
});

export const requestGetListTopSkill = () => ({
  type: "REQUEST_TOP_SKILL_LIST"
});

export const requestGetListTopSkillDone = listSkill => ({
  type: "REQUEST_TOP_SKILL_LIST_DONE",
  listSkill
});

export const requestGetListTopSkillError = error => ({
  type: "REQUEST_TOP_SKILL_LIST_FAIL",
  error: ErrorHandler(error)
});

export const clearDataTopSkill = () => ({
  type: "CLEAR_DATA_TOP_SKILL"
});

export function getListTopUser(dateBegin, page) {
  const token = localStorage.getItem("userToken");
  if (token === null) {
    return dispatch => {
      dispatch(requestGetListTopUserError("Token not found"));
    };
  }
  return dispatch => {
    dispatch(requestGetListTopUser());
    const config = {
      headers: {
        "content-type": "application/json",
        "Access-Control-Allow-Origin": "*",
        Authorization: `Bearer ${token}`
      }
    };

    let path = `${HostAPI}/benefit/top?page=${page}&limit=${limitItem}`;
    if (dateBegin) {
      path = `${HostAPI}/benefit/top?page=${page}&dateBegin=${dateBegin}&limit=${limitItem}`;
    }
    axios.get(path, config).then(
      response => {
        if (response.status !== 200) {
          if (response.status !== 204) {
            dispatch(requestGetListTopUserError(response.data));
          } else {
            dispatch(requestGetListTopUser());
          }
        } else {
          dispatch(requestGetListTopUserDone(response.data));
        }
      },
      err => {
        console.log(err);
        if (err.response) {
          dispatch(requestGetListTopUserError(err.response.data.message));
        } else {
          dispatch(requestGetListTopUserError(err.message));
        }
      }
    );
  };
}

export function getListTopSkill(dateBegin, page) {
  const token = localStorage.getItem("userToken");
  if (token === null) {
    return dispatch => {
      dispatch(requestGetListTopSkillError("Token not found"));
    };
  }
  return dispatch => {
    dispatch(requestGetListTopSkill());
    const config = {
      headers: {
        "content-type": "application/json",
        "Access-Control-Allow-Origin": "*",
        Authorization: `Bearer ${token}`
      }
    };

    let path = `${HostAPI}/benefit/top-skill?page=${page}&limit=${limitItem}`;
    if (dateBegin) {
      path = `${HostAPI}/benefit/top-skill?page=${page}&dateBegin=${dateBegin}&limit=${limitItem}`;
    }
    axios.get(path, config).then(
      response => {
        if (response.status !== 200) {
          if (response.status !== 204) {
            dispatch(requestGetListTopSkillError(response.data));
          } else {
            dispatch(requestGetListTopSkill());
          }
        } else {
          dispatch(requestGetListTopSkillDone(response.data));
        }
      },
      err => {
        console.log(err);
        if (err.response) {
          dispatch(requestGetListTopSkillError(err.response.data.message));
        } else {
          dispatch(requestGetListTopSkillError(err.message));
        }
      }
    );
  };
}

export function getDataBenefit(dateBegin, dateEnd, type) {
  const token = localStorage.getItem("userToken");
  if (token === null) {
    return dispatch => {
      dispatch(requestGetListTopUserError("Token not found"));
    };
  }
  return dispatch => {
    dispatch(requestGetListTopUser());
    const config = {
      headers: {
        "content-type": "application/json",
        "Access-Control-Allow-Origin": "*",
        Authorization: `Bearer ${token}`
      }
    };
    axios
      .get(
        `${HostAPI}/benefit/amount?type=${type}&dateBegin=${dateBegin}&dateEnd=${dateEnd}`,
        config
      )
      .then(
        response => {
          if (response.status !== 200) {
            if (response.status !== 204) {
              dispatch(requestGetListTopUserError(response.data));
            } else {
              dispatch(requestGetListTopUser());
            }
          } else {
            dispatch(requestGetListTopUserDone(response.data));
          }
        },
        err => {
          console.log(err);
          if (err.response) {
            dispatch(requestGetListTopUserError(err.response.data.message));
          } else {
            dispatch(requestGetListTopUserError(err.message));
          }
        }
      );
  };
}
