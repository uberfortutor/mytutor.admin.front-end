import { HostAPI, limitItem, ErrorHandler } from "../config";

const axios = require("axios");

export const requestGetListUser = () => ({
  type: "REQUEST_LIST_USER"
});

export const requestGetListUserDone = listUser => ({
  type: "REQUEST_LIST_USER_DONE",
  listUser
});

export const requestGetListUserError = error => ({
  type: "REQUEST_LIST_USER_FAIL",
  error: ErrorHandler(error)
});

export const requestLockUser = () => ({
  type: "REQUEST_LOCK_USER"
});

export const requestLockUserDone = () => ({
  type: "REQUEST_LOCK_USER_DONE"
});

export const requestLockUserError = error => ({
  type: "REQUEST_LOCK_USER_FAIL",
  error: ErrorHandler(error)
});

export const requestUnlockUser = () => ({
  type: "REQUEST_UNLOCK_USER"
});

export const requestUnlockUserDone = () => ({
  type: "REQUEST_UNLOCK_USER_DONE"
});

export const requestUnlockUserError = error => ({
  type: "REQUEST_UNLOCK_USER_FAIL",
  error: ErrorHandler(error)
});

export const changeStatusUser = userId => ({
  type: "CHANGE_STATUS_USER",
  userId
});

export const clearDataUser = () => ({
  type: "CLEAR_DATA_USER"
});

export const requestGetUserDetail = () => ({
  type: "REQUEST_START"
});

export const requestGetUserDetailDone = data => ({
  type: "REQUEST_DONE",
  data
});

export const requestGetUserDetailError = error => ({
  type: "REQUEST_FAIL",
  error: ErrorHandler(error)
});

export const requestGetUserDetailRefresh = () => ({
  type: "REQUEST_REFRESH"
});

export function getListUser(page, userType, searchName) {
  const token = localStorage.getItem("userToken");
  if (token === null) {
    return dispatch => {
      dispatch(requestGetListUserError("Token not found"));
    };
  }
  return dispatch => {
    dispatch(requestGetListUser());
    const config = {
      headers: {
        "content-type": "application/json",
        "Access-Control-Allow-Origin": "*",
        Authorization: `Bearer ${token}`
      }
    };
    axios
      .get(
        `${HostAPI}/users/list?page=${page}&limit=${limitItem}&type=${userType ||
          ""}&searchName=${searchName || ""}`,
        config
      )
      .then(
        response => {
          if (response.status !== 200) {
            if (response.status !== 204) {
              dispatch(requestGetListUserError(response.data));
            } else {
              dispatch(requestGetListUser());
            }
          } else {
            dispatch(requestGetListUserDone(response.data));
          }
        },
        err => {
          console.log(err);
          if (err.response && err.response.data) {
            dispatch(requestGetListUserError(err.response.data.message));
          } else {
            dispatch(requestGetListUserError(err.message));
          }
        }
      );
  };
}

export function lockUser(id) {
  const token = localStorage.getItem("userToken");
  if (token === null) {
    return dispatch => {
      dispatch(requestLockUserError("Token not found"));
    };
  }

  return dispatch => {
    dispatch(requestLockUser());
    const config = {
      headers: {
        "content-type": "application/json",
        "Access-Control-Allow-Origin": "*",
        Authorization: `Bearer ${token}`
      }
    };
    axios.post(`${HostAPI}/users/lock`, { id }, config).then(
      response => {
        if (response.status !== 200) {
          if (response.status !== 204) {
            dispatch(requestLockUserError());
          } else {
            dispatch(requestLockUser());
          }
        } else {
          dispatch(requestLockUserDone(response.data));
        }
      },
      err => {
        console.log(err);
        dispatch(requestLockUserError("Không thể khóa tài khoản"));
      }
    );
  };
}

export function unlockUser(id) {
  const token = localStorage.getItem("userToken");
  if (token === null) {
    return dispatch => {
      dispatch(requestUnlockUserError("Token not found"));
    };
  }

  return dispatch => {
    dispatch(requestUnlockUser());
    const config = {
      headers: {
        "content-type": "application/json",
        "Access-Control-Allow-Origin": "*",
        Authorization: `Bearer ${token}`
      }
    };
    axios.post(`${HostAPI}/users/unlock`, { id }, config).then(
      response => {
        if (response.status !== 200) {
          if (response.status !== 204) {
            dispatch(requestUnlockUserError(response.data));
          } else {
            dispatch(requestUnlockUser());
          }
        } else {
          dispatch(requestUnlockUserDone());
        }
      },
      err => {
        console.log(err);
        dispatch(requestUnlockUserError("Không thể mở khóa tài khoản"));
      }
    );
  };
}

export function getUserDetail(userId) {
  const token = localStorage.getItem("userToken");
  if (token === null) {
    return dispatch => {
      dispatch(requestGetUserDetailError("Token not found"));
    };
  }
  return dispatch => {
    dispatch(requestGetUserDetail());
    const config = {
      headers: {
        "content-type": "application/json",
        "Access-Control-Allow-Origin": "*",
        Authorization: `Bearer ${token}`
      }
    };
    axios.get(`${HostAPI}/users/detail?id=${userId}`, config).then(
      response => {
        if (response.status !== 200) {
          if (response.status !== 204) {
            dispatch(requestGetUserDetailError(response.data));
          } else {
            dispatch(requestGetUserDetail());
          }
        } else {
          dispatch(requestGetUserDetailDone(response.data));
        }
      },
      err => {
        console.log(err);
        if (err.response && err.response.data) {
          dispatch(requestGetUserDetailError(err.response.data.message));
        } else {
          dispatch(requestGetUserDetailError(err.message));
        }
      }
    );
  };
}
