import { HostAPI, limitItem, ErrorHandler } from "../config";

const axios = require("axios");

export const requestGetListSkill = () => ({
  type: "REQUEST_LIST_SKILL"
});

export const requestGetListSkillDone = listTagSkill => ({
  type: "REQUEST_LIST_SKILL_DONE",
  listTagSkill
});

export const requestGetListSkillError = error => ({
  type: "REQUEST_LIST_SKILL_FAIL",
  error: ErrorHandler(error)
});

export const requestAddSkill = () => ({
  type: "REQUEST_START"
});

export const requestAddSkillDone = () => ({
  type: "REQUEST_DONE"
});

export const requestAddSkillError = error => ({
  type: "REQUEST_FAIL",
  error: ErrorHandler(error)
});

export const requestAddSkillRefresh = () => ({
  type: "REQUEST_REFRESH"
});

export const requestDeleteSkill = () => ({
  type: "REQUEST_START"
});

export const requestDeleteSkillDone = () => ({
  type: "REQUEST_DONE",
  isRefresh: true
});

export const requestDeleteSkillError = error => ({
  type: "REQUEST_FAIL",
  error: ErrorHandler(error)
});

export const requestDeleteSkillRefresh = () => ({
  type: "REQUEST_REFRESH"
});

export const changeStatusSkill = skillId => ({
  type: "CHANGE_STATUS_SKILL",
  skillId
});

export const clearDataTagSkill = () => ({
  type: "CLEAR_DATA_SKILL"
});

export function getListTagKill(page) {
  const token = localStorage.getItem("userToken");
  if (token === null) {
    return dispatch => {
      dispatch(requestGetListSkillError("Token not found"));
    };
  }
  return dispatch => {
    dispatch(requestGetListSkill());
    const config = {
      headers: {
        "content-type": "application/json",
        "Access-Control-Allow-Origin": "*",
        Authorization: `Bearer ${token}`
      }
    };
    axios
      .get(`${HostAPI}/tagskill/list?page=${page}&limit=${limitItem}`, config)
      .then(
        response => {
          if (response.status !== 200) {
            if (response.status !== 204) {
              dispatch(requestGetListSkillError(response.data));
            } else {
              dispatch(requestGetListSkill());
            }
          } else {
            dispatch(requestGetListSkillDone(response.data));
          }
        },
        err => {
          console.log(err);
          if (err.response && err.response.data) {
            dispatch(requestGetListSkillError(err.response.data.message));
          } else {
            dispatch(requestGetListSkillError(err.message));
          }
        }
      );
  };
}

export function addSkill(skill) {
  const token = localStorage.getItem("userToken");
  if (token === null) {
    return dispatch => {
      dispatch(requestAddSkillError("Token not found"));
    };
  }
  return dispatch => {
    dispatch(requestAddSkill());
    const config = {
      headers: {
        "content-type": "application/json",
        "Access-Control-Allow-Origin": "*",
        Authorization: `Bearer ${token}`
      }
    };
    axios.post(`${HostAPI}/tagskill/add`, skill, config).then(
      response => {
        if (response.status !== 200) {
          if (response.status !== 204) {
            dispatch(requestAddSkillError(response.data));
          } else {
            dispatch(requestAddSkill());
          }
        } else {
          dispatch(requestAddSkillDone(response.data));
        }
      },
      err => {
        console.log(err);
        if (err.response && err.response.data) {
          dispatch(requestAddSkillError(err.response.data.message));
        } else {
          dispatch(requestAddSkillError(err.message));
        }
      }
    );
  };
}

export function deleteSkill(id) {
  const token = localStorage.getItem("userToken");
  if (token === null) {
    return dispatch => {
      dispatch(requestDeleteSkillError("Token not found"));
    };
  }
  return dispatch => {
    dispatch(requestDeleteSkill());
    const config = {
      headers: {
        "content-type": "application/json",
        "Access-Control-Allow-Origin": "*",
        Authorization: `Bearer ${token}`
      }
    };
    axios.post(`${HostAPI}/tagskill/delete`, { id }, config).then(
      response => {
        if (response.status !== 200) {
          if (response.status !== 204) {
            dispatch(requestDeleteSkillError(response.data));
          } else {
            dispatch(requestDeleteSkill());
          }
        } else {
          dispatch(requestDeleteSkillDone(response.data));
        }
      },
      err => {
        console.log(err);
        if (err.response && err.response.data) {
          dispatch(requestDeleteSkillError(err.response.data.message));
        } else {
          dispatch(requestDeleteSkillError(err.message));
        }
      }
    );
  };
}
