import { HostAPI, ErrorHandler } from "../config";

const axios = require("axios");

export const requestPostPing = () => ({
  type: "REQUEST_PING_START"
});

export const requestPostPingDone = role => ({
  type: "REQUEST_PING_DONE",
  role
});

export const requestPostPingError = error => ({
  type: "REQUEST_PING_FAIL",
  error: ErrorHandler(error)
});

export const requestPostPingRefresh = () => ({
  type: "REQUEST_PING_REFRESH"
});

export function ping() {
  const token = localStorage.getItem("userToken");
  if (token === null) {
    return dispatch => {
      dispatch(requestPostPingError("Ping failed"));
    };
  }
  return dispatch => {
    dispatch(requestPostPing());
    const config = {
      headers: {
        "content-type": "application/json",
        "Access-Control-Allow-Origin": "*",
        Authorization: `Bearer ${token}`
      }
    };
    axios.post(`${HostAPI}/users/ping`, null, config).then(
      response => {
        if (response.status !== 200) {
          if (response.status !== 204) {
            dispatch(requestPostPingError("Ping failed"));
          } else {
            dispatch(requestPostPing());
          }
        } else {
          dispatch(requestPostPingDone(response.data.role));
        }
      },
      err => {
        console.log(err);
        dispatch(requestPostPingError("Ping failed"));
      }
    );
  };
}
