import { HostAPI, limitItem, ErrorHandler } from "../config";

const axios = require("axios");

export const requestGetListComplain = () => ({
  type: "REQUEST_LIST_COMPLAIN"
});

export const requestGetListComplainDone = listComplain => ({
  type: "REQUEST_LIST_COMPLAIN_DONE",
  listComplain
});

export const requestGetListComplainError = error => ({
  type: "REQUEST_LIST_COMPLAIN_FAIL",
  error: ErrorHandler(error)
});

export const requestUpdateComplain = () => ({
  type: "REQUEST_START"
});

export const requestUpdateComplainDone = () => ({
  type: "REQUEST_DONE",
  isRefresh: true
});

export const requestUpdateComplainError = error => ({
  type: "REQUEST_FAIL",
  error: ErrorHandler(error)
});

export const requestUpdateComplainRefresh = () => ({
  type: "REQUEST_REFRESH"
});

export const changeStatusComplain = complainId => ({
  type: "CHANGE_STATUS_COMPLAIN",
  complainId
});

export const clearDataComplain = () => ({
  type: "CLEAR_COMPLAIN"
});

export const requestGetComplainDetail = () => ({
  type: "REQUEST_START"
});

export const requestGetComplainDetailDone = data => ({
  type: "REQUEST_DONE",
  data
});

export const requestGetComplainDetailError = error => ({
  type: "REQUEST_FAIL",
  error: ErrorHandler(error)
});

export const requestGetComplainDetailRefresh = () => ({
  type: "REQUEST_REFRESH"
});

export function getListComplain(page, status, searchText) {
  const token = localStorage.getItem("userToken");
  if (token === null) {
    return dispatch => {
      dispatch(requestGetListComplainError("Token not found"));
    };
  }
  return dispatch => {
    dispatch(requestGetListComplain());
    const config = {
      headers: {
        "content-type": "application/json",
        "Access-Control-Allow-Origin": "*",
        Authorization: `Bearer ${token}`
      }
    };
    let pathGet = `${HostAPI}/complains/list?page=${page}&limit=${limitItem}&searchText=${searchText ||
      ""}`;
    if (status !== undefined) {
      pathGet = `${HostAPI}/complains/list?page=${page}&limit=${limitItem}&status=${status}&searchText=${searchText ||
        ""}`;
    }
    axios.get(pathGet, config).then(
      response => {
        if (response.status !== 200) {
          if (response.status !== 204) {
            dispatch(requestGetListComplainError(response.data));
          } else {
            dispatch(requestGetListComplain());
          }
        } else {
          dispatch(requestGetListComplainDone(response.data));
        }
      },
      err => {
        console.log(err);
        if (err.response && err.response.data) {
          dispatch(requestGetListComplainError(err.response.data.message));
        } else {
          dispatch(requestGetListComplainError(err.message));
        }
      }
    );
  };
}

export function updateComplainStatus(id) {
  const token = localStorage.getItem("userToken");
  if (token === null) {
    return dispatch => {
      dispatch(requestUpdateComplainError("Token not found"));
    };
  }

  return dispatch => {
    dispatch(requestUpdateComplain());
    const config = {
      headers: {
        "content-type": "application/json",
        "Access-Control-Allow-Origin": "*",
        Authorization: `Bearer ${token}`
      }
    };
    axios
      .post(`${HostAPI}/complains/update-status`, { id, status: 1 }, config)
      .then(
        response => {
          if (response.status !== 200) {
            if (response.status !== 204) {
              dispatch(requestUpdateComplainError());
            } else {
              dispatch(requestUpdateComplain());
            }
          } else {
            dispatch(requestUpdateComplainDone(response.data));
          }
        },
        err => {
          console.log(err);
          dispatch(requestUpdateComplainError("Không thể cập nhật trạng thái"));
        }
      );
  };
}

export function getComplainDetail(idComplain) {
  const token = localStorage.getItem("userToken");
  if (token === null) {
    return dispatch => {
      dispatch(requestGetComplainDetailError("Token not found"));
    };
  }
  return dispatch => {
    dispatch(requestGetComplainDetail());
    const config = {
      headers: {
        "content-type": "application/json",
        "Access-Control-Allow-Origin": "*",
        Authorization: `Bearer ${token}`
      }
    };
    axios.get(`${HostAPI}/complains/detail?id=${idComplain}`, config).then(
      response => {
        if (response.status !== 200) {
          if (response.status !== 204) {
            dispatch(requestGetComplainDetailError(response.data));
          } else {
            dispatch(requestGetComplainDetail());
          }
        } else {
          dispatch(requestGetComplainDetailDone(response.data));
        }
      },
      err => {
        console.log(err);
        if (err.response && err.response.data) {
          dispatch(requestGetComplainDetailError(err.response.data.message));
        } else {
          dispatch(requestGetComplainDetailError(err.message));
        }
      }
    );
  };
}
