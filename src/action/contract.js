import { HostAPI, limitItem, ErrorHandler } from "../config";

const axios = require("axios");

export const requestGetListContract = () => ({
  type: "REQUEST_LIST_CONTRACT"
});

export const requestGetListContractDone = listContract => ({
  type: "REQUEST_LIST_CONTRACT_DONE",
  listContract
});

export const requestGetListContractError = error => ({
  type: "REQUEST_LIST_CONTRACT_FAIL",
  error: ErrorHandler(error)
});

export const requestUpdateContract = () => ({
  type: "REQUEST_START"
});

export const requestUpdateContractDone = () => ({
  type: "REQUEST_DONE",
  isRefresh: true
});

export const requestUpdateContractError = error => ({
  type: "REQUEST_FAIL",
  error: ErrorHandler(error)
});

export const requestUpdateContractRefresh = () => ({
  type: "REQUEST_REFRESH"
});

export const changeStatusContract = contractId => ({
  type: "CHANGE_STATUS_CONTRACT",
  contractId
});

export const clearDataContract = () => ({
  type: "CLEAR_CONTRACT"
});

export function getListContract(page, status, searchText) {
  const token = localStorage.getItem("userToken");
  if (token === null) {
    return dispatch => {
      dispatch(requestGetListContractError("Token not found"));
    };
  }
  return dispatch => {
    dispatch(requestGetListContract());
    const config = {
      headers: {
        "content-type": "application/json",
        "Access-Control-Allow-Origin": "*",
        Authorization: `Bearer ${token}`
      }
    };
    let pathGet = `${HostAPI}/contracts/list?page=${page}&limit=${limitItem}&searchText=${searchText ||
      ""}`;
    if (status !== undefined) {
      pathGet = `${HostAPI}/contracts/list?page=${page}&limit=${limitItem}&status=${status}&searchText=${searchText ||
        ""}`;
    }
    axios.get(pathGet, config).then(
      response => {
        if (response.status !== 200) {
          if (response.status !== 204) {
            dispatch(requestGetListContractError(response.data));
          } else {
            dispatch(requestGetListContract());
          }
        } else {
          dispatch(requestGetListContractDone(response.data));
        }
      },
      err => {
        console.log(err);
        if (err.response && err.response.data) {
          dispatch(requestGetListContractError(err.response.data.message));
        } else {
          dispatch(requestGetListContractError(err.message));
        }
      }
    );
  };
}

export function updateContractStatus(id) {
  const token = localStorage.getItem("userToken");
  if (token === null) {
    return dispatch => {
      dispatch(requestUpdateContractError("Token not found"));
    };
  }

  return dispatch => {
    dispatch(requestUpdateContract());
    const config = {
      headers: {
        "content-type": "application/json",
        "Access-Control-Allow-Origin": "*",
        Authorization: `Bearer ${token}`
      }
    };
    axios.post(`${HostAPI}/contracts/cancel`, { id }, config).then(
      response => {
        if (response.status !== 200) {
          if (response.status !== 204) {
            dispatch(requestUpdateContractError());
          } else {
            dispatch(requestUpdateContract());
          }
        } else {
          dispatch(requestUpdateContractDone(response.data));
        }
      },
      err => {
        console.log(err);
        dispatch(requestUpdateContractError("Không thể cập nhật trạng thái"));
      }
    );
  };
}
