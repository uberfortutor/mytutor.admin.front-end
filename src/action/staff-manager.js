import { HostAPI, limitItem, ErrorHandler } from "../config";

const axios = require("axios");

export const requestGetListStaff = () => ({
  type: "REQUEST_LIST_STAFF"
});

export const requestGetListStaffDone = listStaff => ({
  type: "REQUEST_LIST_STAFF_DONE",
  listStaff
});

export const requestGetListStaffError = error => ({
  type: "REQUEST_LIST_STAFF_FAIL",
  error: ErrorHandler(error)
});

export const requestAddStaff = () => ({
  type: "REQUEST_START"
});

export const requestAddStaffDone = () => ({
  type: "REQUEST_DONE"
});

export const requestAddStaffError = error => ({
  type: "REQUEST_FAIL",
  error: ErrorHandler(error)
});

export const requestAddStaffRefresh = () => ({
  type: "REQUEST_REFRESH"
});

export const clearDataStaff = () => ({
  type: "CLEAR_DATA_STAFF"
});

export const requestChangeStatusStaff = () => ({
  type: "REQUEST_CHANGE_STATUS_STAFF"
});

export const requestChangeStatusStaffDone = () => ({
  type: "REQUEST_CHANGE_STATUS_STAFF_DONE"
});

export const requestChangeStatusStaffError = error => ({
  type: "REQUEST_CHANGE_STATUS_STAFF_FAIL",
  error: ErrorHandler(error)
});

export const changeStatusStaff = staffId => ({
  type: "CHANGE_STATUS_STAFF",
  staffId
});

export const requestResetPassStaff = () => ({
  type: "REQUEST_RESET_PASS_STAFF"
});

export const requestResetPassStaffDone = () => ({
  type: "REQUEST_RESET_PASS_STAFF_DONE"
});

export const requestResetPassStaffError = error => ({
  type: "REQUEST_RESET_PASS_STAFF_FAIL",
  error: ErrorHandler(error)
});

export const resetPassStaffStatus = staffId => ({
  type: "RESET_PASS_STAFF_STATUS",
  staffId
});

export function getListStaff(page, roleFilter) {
  const token = localStorage.getItem("userToken");
  if (token === null) {
    return dispatch => {
      dispatch(requestGetListStaffError("Token not found"));
    };
  }
  return dispatch => {
    dispatch(requestGetListStaff());
    const config = {
      headers: {
        "content-type": "application/json",
        "Access-Control-Allow-Origin": "*",
        Authorization: `Bearer ${token}`
      }
    };
    axios
      .get(
        `${HostAPI}/staff/list?page=${page}&limit=${limitItem}&role=${
          roleFilter >= 0 ? roleFilter : -1
        }`,
        config
      )
      .then(
        response => {
          if (response.status !== 200) {
            if (response.status !== 204) {
              dispatch(requestGetListStaffError(response.data));
            } else {
              dispatch(requestGetListStaff());
            }
          } else {
            dispatch(requestGetListStaffDone(response.data));
          }
        },
        err => {
          dispatch(requestGetListStaffError(err.message));
        }
      );
  };
}

export function addStaff(staff) {
  const token = localStorage.getItem("userToken");
  if (token === null) {
    return dispatch => {
      dispatch(requestAddStaffError("Token not found"));
    };
  }
  return dispatch => {
    dispatch(requestAddStaff());
    const config = {
      headers: {
        "content-type": "application/json",
        "Access-Control-Allow-Origin": "*",
        Authorization: `Bearer ${token}`
      }
    };
    axios.post(`${HostAPI}/staff/add`, staff, config).then(
      response => {
        if (response.status !== 200) {
          if (response.status !== 204) {
            dispatch(requestAddStaffError(response.data));
          } else {
            dispatch(requestAddStaff());
          }
        } else {
          dispatch(requestAddStaffDone(response.data));
        }
      },
      err => {
        console.log(err);
        if (err.response && err.response.data) {
          dispatch(requestAddStaffError(err.response.data.message));
        } else {
          dispatch(requestAddStaffError(err.message));
        }
      }
    );
  };
}

export function changeStatus(id, status) {
  const token = localStorage.getItem("userToken");
  if (token === null) {
    return dispatch => {
      dispatch(requestChangeStatusStaffError("Token not found"));
    };
  }

  return dispatch => {
    dispatch(requestChangeStatusStaff());
    const config = {
      headers: {
        "content-type": "application/json",
        "Access-Control-Allow-Origin": "*",
        Authorization: `Bearer ${token}`
      }
    };
    axios.post(`${HostAPI}/staff/change-status`, { id, status }, config).then(
      response => {
        if (response.status !== 200) {
          if (response.status !== 204) {
            dispatch(requestChangeStatusStaffError());
          } else {
            dispatch(requestChangeStatusStaff());
          }
        } else {
          dispatch(requestChangeStatusStaffDone(response.data));
        }
      },
      err => {
        console.log(err);
        dispatch(requestChangeStatusStaffError("Không thể đổi trạng thái"));
      }
    );
  };
}

export function resetPasswordStaff(id) {
  const token = localStorage.getItem("userToken");
  if (token === null) {
    return dispatch => {
      dispatch(requestResetPassStaffError("Token not found"));
    };
  }

  return dispatch => {
    dispatch(requestResetPassStaff());
    const config = {
      headers: {
        "content-type": "application/json",
        "Access-Control-Allow-Origin": "*",
        Authorization: `Bearer ${token}`
      }
    };
    axios.post(`${HostAPI}/staff/reset-pass`, { id }, config).then(
      response => {
        if (response.status !== 200) {
          if (response.status !== 204) {
            dispatch(requestResetPassStaffError());
          } else {
            dispatch(requestResetPassStaff());
          }
        } else {
          dispatch(requestResetPassStaffDone(response.data));
        }
      },
      err => {
        console.log(err);
        dispatch(requestResetPassStaffError("Không thể đổi trạng thái"));
      }
    );
  };
}
