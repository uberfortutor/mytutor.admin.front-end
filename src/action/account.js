import { HostAPI, ErrorHandler } from "../config";

const axios = require("axios");

export const requestGetAccountInfo = () => ({
  type: "REQUEST_START"
});

export const requestGetAccountInfoDone = data => ({
  type: "REQUEST_DONE",
  data
});

export const requestGetAccountInfoError = error => ({
  type: "REQUEST_FAIL",
  error: ErrorHandler(error)
});

export const requestGetAccountInfoRefresh = () => ({
  type: "REQUEST_REFRESH"
});

export const requestChangePassword = () => ({
  type: "REQUEST_START"
});

export const requestChangePasswordDone = () => ({
  type: "REQUEST_DONE",
  isRefresh: true
});

export const requestChangePasswordError = error => ({
  type: "REQUEST_ERROR",
  error: ErrorHandler(error)
});

export const requestChangePasswordRefresh = () => ({
  type: "REQUEST_REFRESH"
});

export function getAccountInfo() {
  const token = localStorage.getItem("userToken");
  if (token === null) {
    return dispatch => {
      dispatch(requestGetAccountInfoError("Token not found"));
    };
  }
  return dispatch => {
    dispatch(requestGetAccountInfo());
    const config = {
      headers: {
        "content-type": "application/json",
        "Access-Control-Allow-Origin": "*",
        Authorization: `Bearer ${token}`
      }
    };
    axios.get(`${HostAPI}/account/info`, config).then(
      response => {
        if (response.status !== 200) {
          if (response.status !== 204) {
            dispatch(requestGetAccountInfoError(response.data));
          } else {
            dispatch(requestGetAccountInfo());
          }
        } else {
          dispatch(requestGetAccountInfoDone(response.data));
        }
      },
      err => {
        console.log(err);
        if (err.response && err.response.data) {
          dispatch(requestGetAccountInfoError(err.response.data.message));
        } else {
          dispatch(requestGetAccountInfoError(err.message));
        }
      }
    );
  };
}

export function changePassword(oldPass, newPass) {
  const token = localStorage.getItem("userToken");
  if (token === null) {
    return dispatch => {
      dispatch(requestChangePasswordError("Token not found"));
    };
  }
  return dispatch => {
    dispatch(requestChangePassword());
    const config = {
      headers: {
        "content-type": "application/json",
        "Access-Control-Allow-Origin": "*",
        Authorization: `Bearer ${token}`
      }
    };
    axios
      .post(`${HostAPI}/account/change-pass`, { oldPass, newPass }, config)
      .then(
        response => {
          if (response.status !== 200) {
            if (response.status !== 204) {
              dispatch(requestChangePasswordError(response.data));
            } else {
              dispatch(requestChangePassword());
            }
          } else {
            dispatch(requestChangePasswordDone(response.data));
          }
        },
        err => {
          console.log(err);
          dispatch(requestChangePasswordError("Mật khẩu cũ không chính xác"));
        }
      );
  };
}
