import React from "react";
import { Button, Modal, Form } from "react-bootstrap";
import "../../../content/page-data.css";
import LoadingComponent from "../../loading/LoadingComponent";

function ContractDetailComponent(props) {
  const { show, handleClose, contract } = props;

  if (!contract) {
    return (
      <>
        <Modal show={show} onHide={handleClose}>
          <Modal.Header closeButton>
            <div className="modal-title">Chi tiết</div>
          </Modal.Header>
          <div className="dialog-body">
            <LoadingComponent />
          </div>
        </Modal>
      </>
    );
  }
  return (
    <>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <div className="modal-title">Chi tiết</div>
        </Modal.Header>
        <div className="dialog-body">
          <Form>
            <Form.Group>
              <Form.Label>Người thuê</Form.Label>
              <Form.Control value={contract.finderName} disabled />
            </Form.Group>
            <Form.Group>
              <Form.Label>Người dạy</Form.Label>
              <Form.Control value={contract.tutorName} disabled />
            </Form.Group>
            <Form.Group>
              <Form.Label>Dạy</Form.Label>
              <Form.Control value={contract.teach} disabled />
            </Form.Group>
            <Form.Group>
              <Form.Label>Thời lượng</Form.Label>
              <Form.Control
                value={`${contract.numberOfSession} buổi | ${contract.numberOfHourPerSession}h/buổi`}
                disabled
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Tiền thuê</Form.Label>
              <Form.Control
                value={`${contract.confirmedTuition} VND/giờ`}
                disabled
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Ngày tạo</Form.Label>
              <Form.Control
                value={new Date(contract.createdDate).toLocaleDateString()}
                disabled
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Trạng thái hợp đồng</Form.Label>
              <div className="form-control" style={{ border: "none" }}>
                {// eslint-disable-next-line no-nested-ternary
                contract.status === 0 ? (
                  <span className="text-secondary">
                    <b>Chưa ký</b>
                  </span>
                ) : // eslint-disable-next-line no-nested-ternary
                contract.status === 1 ? (
                  <span className="text-primary">
                    <b>Đang có hiệu lực</b>
                  </span>
                ) : contract.status === 2 ? (
                  <span className="text-success">
                    <b>Đã kết thúc</b>
                  </span>
                ) : (
                  <span className="text-danger">
                    <b>Đã bị hủy</b>
                  </span>
                )}
              </div>
            </Form.Group>
            <div className="button-group-center">
              <Button onClick={handleClose}>Đóng</Button>
            </div>
          </Form>
        </div>
      </Modal>
    </>
  );
}

export default ContractDetailComponent;
