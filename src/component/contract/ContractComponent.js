import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faWindowClose } from "@fortawesome/free-solid-svg-icons";
import { Button, Spinner } from "react-bootstrap";
import PagingControl from "../paging-control";
import DropdownMenu from "../commons/dropdown_menu";
import "../../content/page-data.css";
import LoadingComponent from "../loading/LoadingComponent";
import SearchBox from "../commons/search-box";

function ContractComponent(props) {
  const {
    listContract,
    onChangeStatus,
    currentPage,
    maxPage,
    onNext,
    onPrev,
    onFilter,
    status,
    isRequest,
    onItemClick,
    searchText,
    onSearchChange,
    onSearch
  } = props;

  const dropdownItems = [
    {
      value: "Tất cả",
      onClick: () => onFilter(-1),
      active: status === -1
    },
    {
      value: "Chưa ký",
      onClick: () => onFilter(0),
      active: status === 0
    },
    {
      value: "Đang có hiệu lực",
      onClick: () => onFilter(1),
      active: status === 1
    },
    {
      value: "Đã kết thúc",
      onClick: () => onFilter(2),
      active: status === 2
    },
    {
      value: "Đã bị hủy",
      onClick: () => onFilter(3),
      active: status === 3
    }
  ];

  return (
    <div className="staff-body">
      <div className="table-data">
        <h3 className="table-title">Danh sách hợp đồng</h3>
        <div className="button-control">
          <SearchBox
            value={searchText}
            onChange={onSearchChange}
            onEnter={onSearch}
          />
        </div>
        <table className="table table-hover">
          <thead className="thead-green">
            <tr>
              <th scope="col">#</th>
              <th scope="col">Người thuê</th>
              <th scope="col">Người dạy</th>
              <th scope="col">Ngày tạo</th>
              <th scope="col">Ngày cập nhật</th>
              <th scope="col">
                <DropdownMenu items={dropdownItems} name="Trạng thái" />
              </th>
              <th scope="col">Xử lý</th>
            </tr>
          </thead>
          <tbody>
            {// eslint-disable-next-line no-nested-ternary
            isRequest ? (
              <tr>
                <td colSpan="7">
                  <div className="cell-center">
                    <LoadingComponent />
                  </div>
                </td>
              </tr>
            ) : listContract === undefined || listContract.length === 0 ? (
              <tr>
                <td colSpan="7">
                  <div className="cell-center">Không có dữ liệu</div>
                </td>
              </tr>
            ) : (
              listContract.map((contract, idx) => (
                <tr
                  // eslint-disable-next-line react/no-array-index-key
                  key={idx}
                  style={{ cursor: "pointer" }}
                  onClick={() => onItemClick(contract)}
                >
                  <td>{contract.id}</td>
                  <td>{contract.finderName}</td>
                  <td>{contract.tutorName}</td>
                  <td>{new Date(contract.createdDate).toLocaleDateString()}</td>
                  <td>{new Date(contract.updatedDate).toLocaleDateString()}</td>
                  <td>
                    {// eslint-disable-next-line no-nested-ternary
                    contract.status === 0 ? (
                      <span className="text-secondary">
                        <b>Chưa ký</b>
                      </span>
                    ) : // eslint-disable-next-line no-nested-ternary
                    contract.status === 1 ? (
                      <span className="text-primary">
                        <b>Đang có hiệu lực</b>
                      </span>
                    ) : contract.status === 2 ? (
                      <span className="text-success">
                        <b>Đã kết thúc</b>
                      </span>
                    ) : (
                      <span className="text-danger">
                        <b>Đã bị hủy</b>
                      </span>
                    )}
                  </td>
                  <td>
                    {// eslint-disable-next-line no-nested-ternary
                    contract.isLoading ? (
                      <Spinner animation="grow" variant="danger" />
                    ) : contract.status === 1 ? (
                      <Button
                        onClick={e => onChangeStatus(e, contract.id)}
                        className="btn-danger"
                      >
                        <FontAwesomeIcon icon={faWindowClose} />
                      </Button>
                    ) : (
                      <Button className="btn-danger" disabled>
                        <FontAwesomeIcon icon={faWindowClose} />
                      </Button>
                    )}
                  </td>
                </tr>
              ))
            )}
          </tbody>
        </table>
        {maxPage > 1 ? (
          <div className="pg-bottom">
            <PagingControl
              currentPage={currentPage}
              maxPage={maxPage}
              onNext={onNext}
              onPrev={onPrev}
            />
          </div>
        ) : (
          <></>
        )}
      </div>
    </div>
  );
}

export default ContractComponent;
