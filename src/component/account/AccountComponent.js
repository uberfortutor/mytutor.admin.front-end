import React from "react";
import { Button, Form } from "react-bootstrap";
import "../../content/page-data.css";

function AccountComponent(props) {
  const {
    userInfo,
    onChangePassword,
    onOldPasswordChange,
    onNewPasswordChange,
    isRequest,
    onRequestChangePassword,
    isRequestChangePassword,
    oldPass,
    newPass,
    onChangePasswordCancel,
    error,
    requestDone,
    onLogout
  } = props;

  let button;
  let group;
  let message;
  if (isRequestChangePassword) {
    if (error) {
      message = <Form.Label className="text-danger">{error}</Form.Label>;
    } else if (requestDone) {
      message = (
        <Form.Label className="text-success">
          Đổi mật khẩu thành công
        </Form.Label>
      );
    }

    group = (
      <div>
        <Form>
          <Form.Group controlId="oldPass">
            <Form.Label>Mật khẩu cũ</Form.Label>
            <Form.Control
              type="password"
              value={oldPass}
              onChange={onOldPasswordChange}
            />
          </Form.Group>
          <Form.Group controlId="newPass">
            <Form.Label>Mật khẩu mới</Form.Label>
            <Form.Control
              type="password"
              value={newPass}
              onChange={onNewPasswordChange}
            />
          </Form.Group>
          {message}
        </Form>
      </div>
    );
    if (isRequest) {
      button = (
        <>
          <Button onClick={() => onChangePassword()} disabled>
            Đổi
          </Button>
          <Button
            style={{ marginLeft: 10 }}
            className="btn-secondary"
            onClick={() => onChangePasswordCancel()}
            disabled
          >
            Đóng
          </Button>
        </>
      );
    } else {
      button = (
        <>
          <Button onClick={() => onChangePassword()}>Đổi</Button>
          <Button
            style={{ marginLeft: 10 }}
            className="btn-secondary"
            onClick={() => onChangePasswordCancel()}
          >
            Đóng
          </Button>
        </>
      );
    }
  } else {
    button = (
      <>
        <Button onClick={() => onRequestChangePassword()}>Đổi mật khẩu</Button>
        <Button
          className="btn-secondary"
          onClick={() => onLogout()}
          style={{ marginLeft: 10 }}
        >
          Đăng xuất
        </Button>
      </>
    );
    group = <></>;
  }

  return (
    <div className="staff-body">
      <div className="table-data">
        <h3 className="table-title">Tài khoản</h3>
        <div className="button-control" />
        <div>
          <b>Họ và tên: </b>
          {userInfo.name}
        </div>
        <div>
          <b>Email: </b>
          {userInfo.email}
        </div>
        <div style={{ marginTop: 20, maxWidth: "50%" }}>{group}</div>
        <div style={{ marginTop: 20 }}>{button}</div>
      </div>
    </div>
  );
}

export default AccountComponent;
