import React from "react";
import "../../content/home.css";
import StaffManagerContainer from "../../container/staff-manager/StaffManagerContainer";
import NavLeftComponent from "../nav-left/NavLeftComponent";
import TagSkillContainer from "../../container/tagskill/TagSkillContainer";
import UsersContainer from "../../container/users/UsersContainer";
import AccountContainer from "../../container/account/AccountContainer";
import ComplainContainer from "../../container/complain/ComplainContainer";
import BenefitContainer from "../../container/benefit/BenefitContainer";
import ContractContainer from "../../container/contract/ContractContainer";

const PathMapping = [];
PathMapping.staff = {
  index: 0
};
PathMapping.users = {
  index: 1
};
PathMapping.tagskill = {
  index: 2
};
PathMapping.contract = {
  index: 3
};
PathMapping.complain = {
  index: 4
};
PathMapping.benefit = {
  index: 5
};
PathMapping.account = {
  index: 6
};

function HomeComponent(props) {
  const { path, role } = props;
  const pathMapping = PathMapping[path];

  let content = <StaffManagerContainer role={role} />;
  if (pathMapping.index === 1) {
    content = <UsersContainer />;
  } else if (pathMapping.index === 2) {
    content = <TagSkillContainer />;
  } else if (pathMapping.index === 3) {
    content = <ContractContainer />;
  } else if (pathMapping.index === 4) {
    content = <ComplainContainer />;
  } else if (pathMapping.index === 5) {
    content = <BenefitContainer />;
  } else if (pathMapping.index === 6) {
    content = <AccountContainer />;
  }

  return (
    <div className="home-body">
      <NavLeftComponent selectIndex={pathMapping.index} />
      {content}
    </div>
  );
}

export default HomeComponent;
