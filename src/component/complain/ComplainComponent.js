import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheck } from "@fortawesome/free-solid-svg-icons";
import { Button, Spinner } from "react-bootstrap";
import PagingControl from "../paging-control";
import DropdownMenu from "../commons/dropdown_menu";
import "../../content/page-data.css";
import LoadingComponent from "../loading/LoadingComponent";
import SearchBox from "../commons/search-box";

function ComplainComponent(props) {
  const {
    listComplain,
    onChangeStatus,
    currentPage,
    maxPage,
    onNext,
    onPrev,
    onFilter,
    status,
    isRequest,
    onItemClick,
    searchText,
    onSearchChange,
    onSearch
  } = props;

  const dropdownItems = [
    {
      value: "Tất cả",
      onClick: () => onFilter(-1),
      active: status === -1
    },
    {
      value: "Đã xử lý",
      onClick: () => onFilter(1),
      active: status === 1
    },
    {
      value: "Chưa xử lý",
      onClick: () => onFilter(0),
      active: status === 0
    }
  ];

  return (
    <div className="staff-body">
      <div className="table-data">
        <h3 className="table-title">Danh sách thiếu nại</h3>
        <div className="button-control">
          <SearchBox
            value={searchText}
            onChange={onSearchChange}
            onEnter={onSearch}
          />
        </div>
        <table className="table table-hover">
          <thead className="thead-green">
            <tr>
              <th scope="col">#</th>
              <th scope="col">Người thiếu nại</th>
              <th scope="col">
                <DropdownMenu items={dropdownItems} name="Trạng thái" />
              </th>
              <th scope="col">Xử lý</th>
            </tr>
          </thead>
          <tbody>
            {// eslint-disable-next-line no-nested-ternary
            isRequest ? (
              <tr>
                <td colSpan="6">
                  <div className="cell-center">
                    <LoadingComponent />
                  </div>
                </td>
              </tr>
            ) : listComplain === undefined || listComplain.length === 0 ? (
              <tr>
                <td colSpan="4">
                  <div className="cell-center">Không có dữ liệu</div>
                </td>
              </tr>
            ) : (
              listComplain.map((complain, idx) => (
                <tr
                  // eslint-disable-next-line react/no-array-index-key
                  key={idx}
                  style={{ cursor: "pointer" }}
                  onClick={() => onItemClick(complain.id)}
                >
                  <td>{complain.id}</td>
                  <td>{complain.finderName}</td>
                  <td>
                    {complain.status === 0 ? (
                      <span className="text-warning">
                        <b>Chưa xử lý</b>
                      </span>
                    ) : (
                      <span className="text-success">
                        <b>Đã xử lý</b>
                      </span>
                    )}
                  </td>
                  <td>
                    {// eslint-disable-next-line no-nested-ternary
                    complain.isLoading ? (
                      <Spinner animation="grow" variant="danger" />
                    ) : complain.status === 0 ? (
                      <Button onClick={e => onChangeStatus(e, complain.id)}>
                        <FontAwesomeIcon icon={faCheck} />
                      </Button>
                    ) : (
                      <Button
                        className="btn-secondary"
                        onClick={e => onChangeStatus(e, complain.id)}
                        disabled
                      >
                        <FontAwesomeIcon icon={faCheck} />
                      </Button>
                    )}
                  </td>
                </tr>
              ))
            )}
          </tbody>
        </table>
        {maxPage > 1 ? (
          <div className="pg-bottom">
            <PagingControl
              currentPage={currentPage}
              maxPage={maxPage}
              onNext={onNext}
              onPrev={onPrev}
            />
          </div>
        ) : (
          <></>
        )}
      </div>
    </div>
  );
}

export default ComplainComponent;
