import React from "react";
import { Button, Modal, Form } from "react-bootstrap";
import "../../../content/page-data.css";
import LoadingComponent from "../../loading/LoadingComponent";

function ComplainDetailComponent(props) {
  const { show, handleClose, isRequest, complain } = props;

  if (isRequest || !complain) {
    return (
      <>
        <Modal show={show} onHide={handleClose}>
          <Modal.Header closeButton>
            <div className="modal-title">Chi tiết</div>
          </Modal.Header>
          <div className="dialog-body">
            <LoadingComponent />
          </div>
        </Modal>
      </>
    );
  }
  return (
    <>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <div className="modal-title">Chi tiết</div>
        </Modal.Header>
        <div className="dialog-body">
          <Form>
            <Form.Group>
              <Form.Label>Người thiếu nại</Form.Label>
              <Form.Control
                value={`${complain.finder.username} | ${complain.finder.displayName}`}
                disabled
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Người bị thiếu nại</Form.Label>
              <Form.Control
                value={`${complain.tutor.username} | ${complain.tutor.displayName}`}
                disabled
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Dạy</Form.Label>
              <Form.Control value={complain.complain.teach} disabled />
            </Form.Group>
            <Form.Group>
              <Form.Label>Hợp đồng ký ngày</Form.Label>
              <Form.Control
                value={new Date(
                  complain.complain.createdDate
                ).toLocaleDateString()}
                disabled
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Nội dung thiếu nại</Form.Label>
              <textarea
                className="form-control"
                value={complain.complain.content}
                disabled
              />
            </Form.Group>
            <div className="button-group-center">
              <Button onClick={handleClose}>Đóng</Button>
            </div>
          </Form>
        </div>
      </Modal>
    </>
  );
}

export default ComplainDetailComponent;
