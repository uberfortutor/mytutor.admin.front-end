import React from "react";
import "../../content/page-data.css";
import TopUserContainer from "../../container/benefit/TopUserContainer";
import TopSkillContainer from "../../container/benefit/TopSkillContainer";

function BenefitComponent(props) {
  const {
    type,
    filter,
    filterItems,
    typeItems,
    onFilter,
    onFilterType
  } = props;
  return (
    <div className="staff-body">
      <div className="table-data">
        <h3 className="table-title">Doanh thu</h3>
        <div className="button-control-start">
          <div>
            <span>Thời gian</span>
            <select defaultValue={filter} onChange={onFilter}>
              {filterItems.map((item, idx) => (
                // eslint-disable-next-line react/no-array-index-key
                <option key={idx} value={item.value}>
                  {item.title}
                </option>
              ))}
            </select>
          </div>

          <div style={{ marginLeft: 20 }}>
            <span>Loại</span>
            <select defaultValue={type} onChange={onFilterType}>
              {typeItems.map((item, idx) => (
                // eslint-disable-next-line react/no-array-index-key
                <option key={idx} value={item.value}>
                  {item.title}
                </option>
              ))}
            </select>
          </div>
        </div>
        {// eslint-disable-next-line eqeqeq
        type == 0 ? <TopUserContainer /> : <TopSkillContainer />}
      </div>
    </div>
  );
}

export default BenefitComponent;
