import React from "react";
import "../../content/page-data.css";

function AmountComponent(props) {
  const { listBenefit } = props;
  return (
    <>
      <ul>
        {listBenefit.map(user => (
          <li>
            {user.displayName} - {user.amount}
          </li>
        ))}
      </ul>
    </>
  );
}

export default AmountComponent;
