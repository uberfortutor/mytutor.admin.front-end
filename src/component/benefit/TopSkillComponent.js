import React from "react";
import PagingControl from "../paging-control";
import LoadingComponent from "../loading/LoadingComponent";
import "../../content/page-data.css";

function TopSkillComponent(props) {
  const { listSkill, currentPage, maxPage, onNext, onPrev, isRequest } = props;

  return (
    <>
      <table className="table">
        <thead className="thead-green">
          <tr>
            <th scope="col">#</th>
            <th scope="col">Tên kỹ năng</th>
            <th scope="col">Số tiền (VNĐ)</th>
          </tr>
        </thead>
        <tbody>
          {// eslint-disable-next-line no-nested-ternary
          isRequest ? (
            <tr>
              <td colSpan="3">
                <div className="cell-center">
                  <LoadingComponent />
                </div>
              </td>
            </tr>
          ) : listSkill === undefined || listSkill.length === 0 ? (
            <tr>
              <td colSpan="3">
                <div className="cell-center">Không có dữ liệu</div>
              </td>
            </tr>
          ) : (
            listSkill.map((user, idx) => (
              // eslint-disable-next-line react/no-array-index-key
              <tr key={idx}>
                <td>{idx + 1}</td>
                <td>{user.title}</td>
                <td>{user.amount}</td>
              </tr>
            ))
          )}
        </tbody>
      </table>
      {maxPage > 1 ? (
        <div className="pg-bottom">
          <PagingControl
            currentPage={currentPage}
            maxPage={maxPage}
            onNext={onNext}
            onPrev={onPrev}
          />
        </div>
      ) : (
        <></>
      )}
    </>
  );
}

export default TopSkillComponent;
