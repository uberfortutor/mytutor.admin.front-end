import React from "react";
import PagingControl from "../paging-control";
import LoadingComponent from "../loading/LoadingComponent";
import "../../content/page-data.css";

function TopUserComponent(props) {
  const { listUser, currentPage, maxPage, onNext, onPrev, isRequest } = props;

  return (
    <>
      <table className="table">
        <thead className="thead-green">
          <tr>
            <th scope="col">#</th>
            <th scope="col">Họ và tên</th>
            <th scope="col">Email</th>
            <th scope="col">Số tiền (VNĐ)</th>
          </tr>
        </thead>
        <tbody>
          {// eslint-disable-next-line no-nested-ternary
          isRequest ? (
            <tr>
              <td colSpan="4">
                <div className="cell-center">
                  <LoadingComponent />
                </div>
              </td>
            </tr>
          ) : listUser === undefined || listUser.length === 0 ? (
            <tr>
              <td colSpan="4">
                <div className="cell-center">Không có dữ liệu</div>
              </td>
            </tr>
          ) : (
            listUser.map((user, idx) => (
              // eslint-disable-next-line react/no-array-index-key
              <tr key={idx}>
                <td>{idx + 1}</td>
                <td>{user.displayName}</td>
                <td>{user.email}</td>
                <td>{user.amount}</td>
              </tr>
            ))
          )}
        </tbody>
      </table>
      {maxPage > 1 ? (
        <div className="pg-bottom">
          <PagingControl
            currentPage={currentPage}
            maxPage={maxPage}
            onNext={onNext}
            onPrev={onPrev}
          />
        </div>
      ) : (
        <></>
      )}
    </>
  );
}

export default TopUserComponent;
