import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faLock, faUnlock, faKey } from "@fortawesome/free-solid-svg-icons";
import { Button, Spinner } from "react-bootstrap";
import AddStaffDialogContainer from "../../container/staff-manager/dialog/AddStaffDialogContainer";
import PagingControl from "../paging-control";
import "../../content/page-data.css";
import LoadingComponent from "../loading/LoadingComponent";
import DropdownMenu from "../commons/dropdown_menu";

function StaffManagerComponent(props) {
  const {
    showAddDialog,
    handleCloseAddStaffDialog,
    handleButtonAddClick,
    listStaff,
    currentPage,
    maxPage,
    onNext,
    onPrev,
    isRequest,
    role,
    roleFilter,
    onFilter,
    onChangeStatus,
    onResetPassword
  } = props;

  const dropdownItems = [
    {
      value: "Tất cả",
      onClick: () => onFilter(-1),
      active: roleFilter === -1
    },
    {
      value: "Người quản trị",
      onClick: () => onFilter(0),
      active: roleFilter === 0
    },
    {
      value: "Người kiểm duyệt",
      onClick: () => onFilter(1),
      active: roleFilter === 1
    }
  ];

  return (
    <div className="staff-body">
      <AddStaffDialogContainer
        show={showAddDialog}
        handleClose={handleCloseAddStaffDialog}
      />
      <div className="table-data">
        <h3 className="table-title">Danh sách người quản lý</h3>

        <div className="button-control">
          {// eslint-disable-next-line eqeqeq
          role == 0 ? (
            <Button onClick={handleButtonAddClick}>Thêm</Button>
          ) : (
            <></>
          )}
        </div>
        <table className="table">
          <thead className="thead-green">
            <tr>
              <th scope="col">#</th>
              <th scope="col">Tên đăng nhập</th>
              <th scope="col">Họ và tên</th>
              <th scope="col">Email</th>
              <th scope="col">
                <DropdownMenu items={dropdownItems} name="Chức vụ" />
              </th>
              {// eslint-disable-next-line eqeqeq
              role == 0 ? <th scope="col">Thao tác</th> : <></>}
              <th scope="col"> </th>
            </tr>
          </thead>
          <tbody>
            {// eslint-disable-next-line no-nested-ternary
            isRequest ? (
              <tr>
                <td colSpan="6">
                  <div className="cell-center">
                    <LoadingComponent />
                  </div>
                </td>
              </tr>
            ) : listStaff === undefined || listStaff.length === 0 ? (
              <tr>
                <td colSpan="6">
                  <div className="cell-center">Không có dữ liệu</div>
                </td>
              </tr>
            ) : (
              listStaff.map((staff, idx) => (
                // eslint-disable-next-line react/no-array-index-key
                <tr key={idx}>
                  <td>{staff.id}</td>
                  <td>{staff.username}</td>
                  <td>{staff.name}</td>
                  <td>{staff.email}</td>
                  <td>
                    {staff.role === 1 ? "Người kiểm duyệt" : "Người quản trị"}
                  </td>
                  {// eslint-disable-next-line eqeqeq
                  role == 0 ? (
                    <td>
                      {// eslint-disable-next-line no-nested-ternary
                      staff.lock ? (
                        <div>
                          {staff.isLoading ? (
                            <Spinner animation="grow" variant="primary" />
                          ) : (
                            <Button
                              className="btn-danger"
                              onClick={e =>
                                onChangeStatus(e, staff.id, !staff.lock)
                              }
                            >
                              <FontAwesomeIcon icon={faLock} />
                            </Button>
                          )}
                        </div>
                      ) : (
                        <div>
                          {staff.isLoading ? (
                            <Spinner animation="grow" variant="danger" />
                          ) : (
                            <Button
                              onClick={e =>
                                onChangeStatus(e, staff.id, !staff.lock)
                              }
                            >
                              <FontAwesomeIcon icon={faUnlock} />
                            </Button>
                          )}
                          {staff.isResetPass ? (
                            <Spinner
                              animation="grow"
                              variant="success"
                              style={{ marginLeft: 10 }}
                            />
                          ) : (
                            <Button
                              className="btn-success"
                              onClick={e => onResetPassword(e, staff.id)}
                              style={{ marginLeft: 10 }}
                            >
                              <FontAwesomeIcon icon={faKey} />
                            </Button>
                          )}
                        </div>
                      )}
                    </td>
                  ) : (
                    <></>
                  )}
                  <td> </td>
                </tr>
              ))
            )}
          </tbody>
        </table>
        {maxPage > 1 ? (
          <div className="pg-bottom">
            <PagingControl
              currentPage={currentPage}
              maxPage={maxPage}
              onNext={onNext}
              onPrev={onPrev}
            />
          </div>
        ) : (
          <></>
        )}
      </div>
    </div>
  );
}

export default StaffManagerComponent;
