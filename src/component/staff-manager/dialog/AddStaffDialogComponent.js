import React from "react";
import { Button, Modal, Form } from "react-bootstrap";
import "../../../content/page-data.css";

function AddStaffDialogComponent(props) {
  const { show, error, handleClose, handleAdd, isRequest } = props;
  const {
    handleUsernameChange,
    handleNameChange,
    handlePasswordChange,
    handleRoleChange,
    handleEmailChange
  } = props;

  let messageError;
  if (error !== undefined && error !== null) {
    messageError = (
      <Form.Group>
        <Form.Label className="text-danger">{error}</Form.Label>
      </Form.Group>
    );
  } else {
    messageError = <></>;
  }

  let btnAdd;
  if (isRequest) {
    btnAdd = (
      <Button type="submit" disabled>
        Thêm
      </Button>
    );
  } else {
    btnAdd = <Button type="submit">Thêm</Button>;
  }

  return (
    <>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <div className="modal-title">Thêm quản trị viên</div>
        </Modal.Header>
        <div className="dialog-body">
          <Form onSubmit={handleAdd}>
            <Form.Group controlId="username">
              <Form.Label>Tên đăng nhập</Form.Label>
              <Form.Control
                type="text"
                placeholder="Nhập tên đăng nhập"
                onChange={handleUsernameChange}
                required
              />
            </Form.Group>
            <Form.Group controlId="name">
              <Form.Label>Họ và tên</Form.Label>
              <Form.Control
                type="text"
                placeholder="Nhập họ và tên"
                onChange={handleNameChange}
                required
              />
            </Form.Group>
            <Form.Group controlId="email">
              <Form.Label>Email</Form.Label>
              <Form.Control
                type="text"
                placeholder="Nhập email"
                onChange={handleEmailChange}
                required
              />
            </Form.Group>
            <Form.Group controlId="password">
              <Form.Label>Mật khẩu</Form.Label>
              <Form.Control
                type="password"
                placeholder="Nhập mật khẩu"
                onChange={handlePasswordChange}
                required
              />
            </Form.Group>
            <Form.Group controlId="role">
              <Form.Label>Chức vụ</Form.Label>
              <select
                className="form-control"
                defaultValue="1"
                onChange={handleRoleChange}
              >
                <option value="0">Quản trị</option>
                <option value="1">Người kiểm duyệt</option>
              </select>
            </Form.Group>
            {messageError}
            <div className="button-group-center">{btnAdd}</div>
          </Form>
        </div>
      </Modal>
    </>
  );
}

export default AddStaffDialogComponent;
