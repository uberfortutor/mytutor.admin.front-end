import React from "react";
import { Button, Form } from "react-bootstrap";
import TitleComponent from "../TitleComponent";
import "../../content/form.css";

function LoginComponent(props) {
  const {
    handleSubmit,
    handlePasswordChange,
    handleUsernameChange,
    isRequest,
    error
  } = props;

  let buttonLogin;
  if (isRequest) {
    buttonLogin = (
      <Button variant="primary" type="submit" disabled>
        Đăng nhập
      </Button>
    );
  } else {
    buttonLogin = (
      <Button variant="primary" type="submit">
        Đăng nhập
      </Button>
    );
  }

  let messageError;
  if (error !== undefined && error !== null) {
    messageError = (
      <Form.Group>
        <Form.Label style={{ color: "#ff7373" }}>{error}</Form.Label>
      </Form.Group>
    );
  } else {
    messageError = <></>;
  }

  return (
    <>
      <TitleComponent title="Đăng nhập - Mytutor admin" />
      <div
        style={{
          width: "100%",
          height: "100%",
          backgroundColor: "rgb(56, 102, 119)"
        }}
      >
        <div
          className="form"
          style={{
            height: "100%",
            display: "flex",
            flexDirection: "column",
            justifyContent: "center"
          }}
        >
          <div
            className="text-center"
            style={{ marginBottom: 50, color: "#fff" }}
          >
            <h1>MyTutor Admin</h1>
          </div>
          <Form onSubmit={handleSubmit}>
            <Form.Group controlId="formBasicEmail">
              <Form.Label style={{ color: "#fff" }}>Tên đăng nhập</Form.Label>
              <Form.Control
                type="text"
                placeholder="Nhập username"
                onChange={handleUsernameChange}
              />
            </Form.Group>
            <Form.Group controlId="formBasicPassword">
              <Form.Label style={{ color: "#fff" }}>Mật khẩu</Form.Label>
              <Form.Control
                type="password"
                placeholder="Nhập mật khẩu"
                onChange={handlePasswordChange}
              />
            </Form.Group>
            {messageError}
            <div className="button_group">{buttonLogin}</div>
          </Form>
        </div>
      </div>
    </>
  );
}

export default LoginComponent;
