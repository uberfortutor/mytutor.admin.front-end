import React from "react";
import { Dropdown } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCaretDown } from "@fortawesome/free-solid-svg-icons";
import "../../content/dropdown_menu.css";

const CustomToggle = React.forwardRef(({ children, onClick }, ref) => (
  // eslint-disable-next-line jsx-a11y/no-static-element-interactions
  /* eslint-disable jsx-a11y/no-static-element-interactions */
  // eslint-disable-next-line jsx-a11y/click-events-have-key-events
  <div
    ref={ref}
    onClick={e => {
      e.preventDefault();
      onClick(e);
    }}
  >
    {children}
    <FontAwesomeIcon icon={faCaretDown} className="dropdown-icon" />
  </div>
));

const CustomMenu = React.forwardRef(
  ({ children, style, className, "aria-labelledby": labeledBy }, ref) => {
    return (
      <div
        ref={ref}
        style={style}
        className={className}
        aria-labelledby={labeledBy}
      >
        <ul className="list-unstyled">{children}</ul>
      </div>
    );
  }
);

function DropdownMenu(props) {
  const { items, name } = props;
  return (
    <>
      <Dropdown>
        <Dropdown.Toggle as={CustomToggle}>{name}</Dropdown.Toggle>
        <Dropdown.Menu as={CustomMenu}>
          {items.map((item, idx) => {
            return (
              <Dropdown.Item
                active={item.active}
                // eslint-disable-next-line react/no-array-index-key
                key={idx}
                eventKey={idx}
                onClick={item.onClick}
              >
                {item.value}
              </Dropdown.Item>
            );
          })}
        </Dropdown.Menu>
      </Dropdown>
    </>
  );
}

export default DropdownMenu;
