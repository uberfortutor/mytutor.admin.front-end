import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Container } from "react-bootstrap";
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import "./search-box.styles.scss";

export default function SearchBox({ onEnter, onChange }) {
  const searchInput = React.useRef(null);

  return (
    <Container fluid className="search-box-container">
      <div className="search-box">
        <input
          ref={searchInput}
          placeholder="Tìm kiếm"
          onChange={onChange}
          onKeyPress={event => {
            if (event.key === "Enter") {
              // eslint-disable-next-line no-unused-expressions
              onEnter && onEnter();
            }
          }}
        />
        <FontAwesomeIcon className="icon" icon={faSearch} />
      </div>
    </Container>
  );
}
