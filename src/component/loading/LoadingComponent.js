import React from "react";
import { Spinner } from "react-bootstrap";
import "../../content/loading.css";

function LoadingComponent({ type }) {
  return (
    <div className={type === 2 ? "loading-container-2" : "loading-container"}>
      <Spinner className="spinner" animation="border" variant="primary" />
    </div>
  );
}

export default LoadingComponent;
