import React from "react";
import { Button, Modal, Form, Badge } from "react-bootstrap";
import "../../../content/page-data.css";
import LoadingComponent from "../../loading/LoadingComponent";

function UserDetailComponent(props) {
  const { show, handleClose, isRequest, userDetail } = props;

  if (isRequest || !userDetail) {
    return (
      <>
        <Modal show={show} onHide={handleClose}>
          <Modal.Header closeButton>
            <div className="modal-title">Chi tiết</div>
          </Modal.Header>
          <div className="dialog-body">
            <LoadingComponent />
          </div>
        </Modal>
      </>
    );
  }
  return (
    <>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <div className="modal-title">Chi tiết</div>
        </Modal.Header>
        <div className="dialog-body">
          <Form>
            <Form.Group>
              <Form.Label>Tên tài khoản</Form.Label>
              <Form.Control value={userDetail.user.username} disabled />
            </Form.Group>
            <Form.Group>
              <Form.Label>Họ và tên</Form.Label>
              <Form.Control value={userDetail.user.displayName} disabled />
            </Form.Group>
            <Form.Group>
              <Form.Label>Số điện thoại</Form.Label>
              <Form.Control value={userDetail.user.phone} disabled />
            </Form.Group>
            <Form.Group>
              <Form.Label>Email</Form.Label>
              <Form.Control value={userDetail.user.email} disabled />
            </Form.Group>
            <Form.Group>
              <Form.Label>Loại tài khoản</Form.Label>
              <Form.Control
                value={userDetail.user.tutorId ? "Tutor" : "Finder"}
                disabled
              />
            </Form.Group>
            {userDetail.user.tutorId ? (
              <Form.Group>
                <Form.Label>Kỹ năng</Form.Label>
                <div style={{ marginLeft: 10 }}>
                  {userDetail.skills.length > 0 ? (
                    userDetail.skills.map((v, idx) => (
                      <>
                        {idx !== 0 && idx % 3 === 0 ? <br /> : <></>}
                        <Badge
                          // eslint-disable-next-line react/no-array-index-key
                          key={idx}
                          variant="success"
                          style={{ marginRight: 5 }}
                        >
                          {v.title}
                        </Badge>
                      </>
                    ))
                  ) : (
                    <Form.Label>Chưa có</Form.Label>
                  )}
                </div>
              </Form.Group>
            ) : (
              <></>
            )}
            <div className="button-group-center">
              <Button onClick={handleClose}>Đóng</Button>
            </div>
          </Form>
        </div>
      </Modal>
    </>
  );
}

export default UserDetailComponent;
