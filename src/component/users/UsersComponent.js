import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faLock, faUnlock } from "@fortawesome/free-solid-svg-icons";
import { Button, Spinner } from "react-bootstrap";
import PagingControl from "../paging-control";
import LoadingComponent from "../loading/LoadingComponent";
import "../../content/page-data.css";
import DropdownMenu from "../commons/dropdown_menu";
import SearchBox from "../commons/search-box";

function UserComponent(props) {
  const {
    listUser,
    onUnlock,
    onLock,
    currentPage,
    maxPage,
    onNext,
    onPrev,
    onFilter,
    isRequest,
    userType,
    onItemClick,
    onSearch,
    onSearchChange,
    searchText
  } = props;

  const dropdownItems = [
    {
      value: "Tất cả",
      onClick: () => onFilter(""),
      active: userType === ""
    },
    {
      value: "Tutor",
      onClick: () => onFilter("tutor"),
      active: userType === "tutor"
    },
    {
      value: "Finder",
      onClick: () => onFilter("finder"),
      active: userType === "finder"
    }
  ];

  return (
    <div className="staff-body">
      <div className="table-data">
        <h3 className="table-title">Danh sách người dùng</h3>
        <div className="button-control">
          <SearchBox
            onEnter={onSearch}
            onChange={onSearchChange}
            value={searchText}
          />
        </div>
        <table className="table table-hover">
          <thead className="thead-green">
            <tr>
              <th scope="col">#</th>
              <th scope="col">Tên đăng nhập</th>
              <th scope="col">Họ và tên</th>
              <th scope="col">Email</th>
              <th scope="col">
                <DropdownMenu items={dropdownItems} name="Loại tài khoản" />
              </th>
              <th scope="col">Thao tác</th>
            </tr>
          </thead>
          <tbody>
            {// eslint-disable-next-line no-nested-ternary
            isRequest ? (
              <tr>
                <td colSpan="6">
                  <div className="cell-center">
                    <LoadingComponent />
                  </div>
                </td>
              </tr>
            ) : listUser === undefined || listUser.length === 0 ? (
              <tr>
                <td colSpan="6">
                  <div className="cell-center">Không có dữ liệu</div>
                </td>
              </tr>
            ) : (
              listUser.map((user, idx) => (
                <tr
                  // eslint-disable-next-line react/no-array-index-key
                  key={idx}
                  onClick={() => onItemClick(user)}
                  style={{ cursor: "pointer" }}
                >
                  <td>{user.id}</td>
                  <td>{user.username}</td>
                  <td>{user.displayName}</td>
                  <td>{user.email}</td>
                  <td>{user.tutorId ? "Tutor" : "Finder"}</td>
                  <td>
                    {// eslint-disable-next-line no-nested-ternary
                    user.lock ? (
                      user.isLoading ? (
                        <Spinner animation="grow" variant="primary" />
                      ) : (
                        <Button
                          className="btn-danger"
                          onClick={e => onUnlock(e, user.id)}
                        >
                          <FontAwesomeIcon icon={faLock} />
                        </Button>
                      )
                    ) : user.isLoading ? (
                      <Spinner animation="grow" variant="danger" />
                    ) : (
                      <Button onClick={e => onLock(e, user.id)}>
                        <FontAwesomeIcon icon={faUnlock} />
                      </Button>
                    )}
                  </td>
                </tr>
              ))
            )}
          </tbody>
        </table>
        {maxPage > 1 ? (
          <div className="pg-bottom">
            <PagingControl
              currentPage={currentPage}
              maxPage={maxPage}
              onNext={onNext}
              onPrev={onPrev}
            />
          </div>
        ) : (
          <></>
        )}
      </div>
    </div>
  );
}

export default UserComponent;
