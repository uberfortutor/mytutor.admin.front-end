import React from "react";
import { Button, Spinner } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrash } from "@fortawesome/free-solid-svg-icons";
import AddTagSkillDialogContainer from "../../container/tagskill/dialog/AddTagSkillDialogContainer";
import "../../content/page-data.css";
import PagingControl from "../paging-control";
import LoadingComponent from "../loading/LoadingComponent";

function TagSkillComponent(props) {
  const {
    showAddDialog,
    handleCloseAddTagSkillDialog,
    handleButtonAddClick,
    listTagSkill,
    currentPage,
    maxPage,
    onNext,
    onPrev,
    onDelete,
    isRequest
  } = props;

  return (
    <div className="staff-body">
      <AddTagSkillDialogContainer
        show={showAddDialog}
        handleClose={handleCloseAddTagSkillDialog}
      />
      <div className="table-data">
        <h3 className="table-title">Danh sách nhãn kỹ năng</h3>
        <div className="button-control">
          <Button onClick={handleButtonAddClick}>Thêm</Button>
        </div>
        <table className="table">
          <thead className="thead-green">
            <tr>
              <th scope="col">#</th>
              <th scope="col">Tên kỹ năng</th>
              <th scope="col">Thao tác</th>
            </tr>
          </thead>
          <tbody>
            {// eslint-disable-next-line no-nested-ternary
            isRequest ? (
              <tr>
                <td colSpan="3">
                  <div className="cell-center">
                    <LoadingComponent />
                  </div>
                </td>
              </tr>
            ) : listTagSkill === undefined || listTagSkill.length === 0 ? (
              <tr>
                <td colSpan="3">
                  <div className="cell-center">Không có dữ liệu</div>
                </td>
              </tr>
            ) : (
              listTagSkill.map((skill, idx) => (
                // eslint-disable-next-line react/no-array-index-key
                <tr key={idx}>
                  <td>{skill.id}</td>
                  <td>{skill.title}</td>
                  <td>
                    {skill.isLoading ? (
                      <Spinner animation="grow" variant="danger" />
                    ) : (
                      <Button
                        className="btn-danger"
                        onClick={e => onDelete(e, skill.id)}
                      >
                        <FontAwesomeIcon icon={faTrash} />
                      </Button>
                    )}
                  </td>
                </tr>
              ))
            )}
          </tbody>
        </table>
        {maxPage > 1 ? (
          <div className="pg-bottom">
            <PagingControl
              currentPage={currentPage}
              maxPage={maxPage}
              onNext={onNext}
              onPrev={onPrev}
            />
          </div>
        ) : (
          <></>
        )}
      </div>
    </div>
  );
}

export default TagSkillComponent;
