import React from "react";
import { Button, Modal, Form } from "react-bootstrap";
import "../../../content/page-data.css";

function AddTagSkillDialogComponent(props) {
  const { show, error, handleClose, handleAdd, isRequest } = props;
  const { handleTitleChange } = props;

  let messageError;
  if (error !== undefined && error !== null) {
    messageError = (
      <Form.Group>
        <Form.Label className="text-danger">{error}</Form.Label>
      </Form.Group>
    );
  } else {
    messageError = <></>;
  }

  let btnAdd;
  if (isRequest) {
    btnAdd = (
      <Button type="submit" disabled>
        Thêm
      </Button>
    );
  } else {
    btnAdd = <Button type="submit">Thêm</Button>;
  }

  return (
    <>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <div className="modal-title">Thêm nhãn kỹ năng</div>
        </Modal.Header>
        <div className="dialog-body">
          <Form onSubmit={handleAdd}>
            <Form.Group controlId="username">
              <Form.Label>Tên kỹ năng</Form.Label>
              <Form.Control
                type="text"
                placeholder="Nhập tên kỹ năng"
                onChange={handleTitleChange}
                required
              />
            </Form.Group>
            {messageError}
            <div className="button-group-center">{btnAdd}</div>
          </Form>
        </div>
      </Modal>
    </>
  );
}

export default AddTagSkillDialogComponent;
