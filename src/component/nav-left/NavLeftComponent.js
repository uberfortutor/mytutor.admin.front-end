import React from "react";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faUser,
  faUsers,
  faAddressCard,
  faBuilding,
  faTags,
  faMoneyBill,
  faFileContract
} from "@fortawesome/free-solid-svg-icons";
import "../../content/nav.css";

const arrListItem = [
  <>
    <FontAwesomeIcon icon={faUser} className="icon" />
    Quản lý staff
  </>,
  <>
    <FontAwesomeIcon icon={faUsers} className="icon" />
    Người dùng
  </>,
  <>
    <FontAwesomeIcon icon={faTags} className="icon" />
    Nhãn kỹ năng
  </>,
  <>
    <FontAwesomeIcon icon={faFileContract} className="icon" />
    Hợp đồng
  </>,
  <>
    <FontAwesomeIcon icon={faBuilding} className="icon" />
    Thiếu nại
  </>,
  <>
    <FontAwesomeIcon icon={faMoneyBill} className="icon" />
    Doanh thu
  </>,
  <>
    <FontAwesomeIcon icon={faAddressCard} className="icon" />
    Tài khoản
  </>
];
const arrListLink = [
  "/staff",
  "/users",
  "/tagskill",
  "/contract",
  "/complain",
  "/benefit",
  "/account"
];

function NavLeftComponent(props) {
  const { selectIndex } = props;
  return (
    <>
      <div className="wrapper nav-left">
        <nav id="sidebar">
          <div className="nav-header">
            <h3>Quản trị Mytutor</h3>
          </div>
          <ul className="list-unstyled components">
            {arrListItem.map((item, idx) => (
              // eslint-disable-next-line react/no-array-index-key
              <li key={idx}>
                <Link
                  to={arrListLink[idx]}
                  className={idx === selectIndex ? "link active" : "link"}
                >
                  {item}
                </Link>
              </li>
            ))}
          </ul>
        </nav>
      </div>
    </>
  );
}

export default NavLeftComponent;
