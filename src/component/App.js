import React from "react";
import thunkMiddleware from "redux-thunk";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { createStore, applyMiddleware } from "redux";
import { Provider } from "react-redux";
import rootReducer from "../reducer";
import LoginContainer from "../container/login/LoginContainer";
import HomeContainer from "../container/home/HomeContainer";

const store = createStore(rootReducer, applyMiddleware(thunkMiddleware));

function App() {
  return (
    <Provider store={store}>
      <Router>
        <Switch>
          <Route path="/login">
            <LoginContainer />
          </Route>
          <Route
            path="/:path"
            render={({ match }) => <HomeContainer path={match.params.path} />}
          />
          <Route path="/" render={() => <HomeContainer path="staff" />} />
        </Switch>
      </Router>
    </Provider>
  );
}

export default App;
