const ContractReducer = (
  state = {
    isRequest: false,
    error: null,
    requestDone: false,
    listContract: null,
    totalContract: 0
  },
  action
) => {
  switch (action.type) {
    case "REQUEST_LIST_CONTRACT":
      return {
        ...state,
        isRequest: true,
        error: null,
        requestDone: false
      };
    case "REQUEST_LIST_CONTRACT_DONE":
      return {
        ...state,
        isRequest: false,
        error: null,
        requestDone: true,
        listContract: action.listContract.contracts,
        totalContract: action.listContract.total
      };
    case "REQUEST_LIST_CONTRACT_FAIL":
      return {
        ...state,
        isRequest: false,
        error: action.error,
        requestDone: false,
        listContract: [],
        totalContract: 0
      };
    case "CHANGE_STATUS_CONTRACT": {
      const { listContract } = state;
      const newList = [];
      listContract.forEach(v => {
        if (v.id === action.contractId) {
          newList.push({
            ...v,
            isLoading: true
          });
        } else {
          newList.push(v);
        }
      });
      return {
        ...state,
        listContract: newList
      };
    }
    case "CLEAR_CONTRACT":
      return {
        isRequest: false,
        error: null,
        requestDone: false,
        listContract: null,
        totalContract: 0
      };
    default:
      return state;
  }
};

export default ContractReducer;
