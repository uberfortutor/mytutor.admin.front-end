import { combineReducers } from "redux";
import LoginReducer from "./LoginReducer";
import StaffManagerReducer from "./StaffManagerReducer";
import RequestReducer from "./RequestReducer";
import TagSkillReducer from "./TagSkillReducer";
import UserReducer from "./UsersReducer";
import HomeReducer from "./HomeReducer";
import ComplainReducer from "./ComplainReducer";
import TopUserReducer from "./TopUserReducer";
import AmountReducer from "./AmountReducer";
import BenefitReducer from "./BenefitReducer";
import TopSkillReducer from "./TopSkillReducer";
import ContractReducer from "./ContractReducer";

export default combineReducers({
  LoginReducer,
  StaffManagerReducer,
  RequestReducer,
  TagSkillReducer,
  UserReducer,
  HomeReducer,
  ComplainReducer,
  TopUserReducer,
  AmountReducer,
  BenefitReducer,
  TopSkillReducer,
  ContractReducer
});
