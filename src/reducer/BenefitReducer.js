const BenefitReducer = (
  state = {
    dateBegin: new Date()
      .toJSON()
      .slice(0, 10)
      .replace(/-/g, "/")
  },
  action
) => {
  switch (action.type) {
    case "CHANGE_BEGIN_DATE":
      return {
        ...state,
        dateBegin: action.dateBegin
      };
    case "CLEAR_BEGIN_DATE":
      return {
        ...state,
        dateBegin: new Date()
          .toJSON()
          .slice(0, 10)
          .replace(/-/g, "/")
      };
    default:
      return state;
  }
};

export default BenefitReducer;
