const UsersReducer = (
  state = {
    isRequest: false,
    isRequestListUser: false,
    error: null,
    requestDone: false,
    listUser: null,
    totalUser: 0,
    isRefresh: false
  },
  action
) => {
  switch (action.type) {
    case "REQUEST_LIST_USER":
      return {
        ...state,
        isRequestListUser: true,
        error: null,
        requestDone: false,
        isRefresh: false
      };
    case "REQUEST_LIST_USER_DONE":
      return {
        ...state,
        isRequestListUser: false,
        error: null,
        requestDone: true,
        listUser: action.listUser.users,
        totalUser: action.listUser.total,
        isRefresh: false
      };
    case "REQUEST_LIST_USER_FAIL":
      return {
        ...state,
        isRequestListUser: false,
        error: action.error,
        requestDone: false,
        listUser: [],
        totalUser: 0,
        isRefresh: false
      };
    case "REQUEST_LOCK_USER" || "REQUEST_UNLOCK_USER":
      return {
        ...state,
        isRequest: true,
        error: null,
        requestDone: false,
        isRefresh: false
      };
    case "REQUEST_LOCK_USER_DONE":
    case "REQUEST_UNLOCK_USER_DONE":
      return {
        ...state,
        isRequest: false,
        error: null,
        requestDone: true,
        isRefresh: true
      };
    case "REQUEST_LOCK_USER_FAIL":
    case "REQUEST_UNLOCK_USER_FAIL":
      return {
        ...state,
        isRequest: false,
        error: action.error,
        requestDone: false,
        isRefresh: false
      };
    case "CHANGE_STATUS_USER": {
      const { listUser } = state;
      const newList = [];
      listUser.forEach(v => {
        if (v.id === action.userId) {
          newList.push({
            ...v,
            isLoading: true
          });
        } else {
          newList.push(v);
        }
      });
      return {
        ...state,
        listUser: newList
      };
    }
    case "CLEAR_DATA_USER":
      return {
        isRequestListUser: false,
        isRequest: false,
        error: null,
        requestDone: false,
        listUser: null,
        totalUser: 0,
        isRefresh: false
      };
    default:
      return state;
  }
};

export default UsersReducer;
