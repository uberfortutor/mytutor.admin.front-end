const StaffManagerReducer = (
  state = {
    isRequest: false,
    isRequestListStaff: false,
    error: null,
    requestDone: false,
    listStaff: null,
    isRefresh: false,
    totalStaff: 0
  },
  action
) => {
  switch (action.type) {
    case "REQUEST_LIST_STAFF":
      return {
        ...state,
        isRequestListStaff: true,
        error: null,
        requestDone: false,
        isRefresh: false
      };
    case "REQUEST_LIST_STAFF_DONE":
      return {
        ...state,
        isRequestListStaff: false,
        error: null,
        requestDone: true,
        listStaff: action.listStaff.staffs,
        totalStaff: action.listStaff.total,
        isRefresh: false
      };
    case "REQUEST_LIST_STAFF_FAIL":
      return {
        ...state,
        isRequestListStaff: false,
        error: action.error,
        requestDone: false,
        listStaff: [],
        totalStaff: 0,
        isRefresh: false
      };
    case "REQUEST_CHANGE_STATUS_STAFF":
    case "REQUEST_RESET_PASS_STAFF":
      return {
        ...state,
        isRequest: true,
        error: null,
        requestDone: false,
        isRefresh: false
      };
    case "REQUEST_CHANGE_STATUS_STAFF_DONE":
    case "REQUEST_RESET_PASS_STAFF_DONE":
      return {
        ...state,
        isRequest: false,
        error: null,
        requestDone: true,
        isRefresh: true
      };
    case "REQUEST_CHANGE_STATUS_STAFF_FAIL":
    case "REQUEST_RESET_PASS_STAFF_FAIL":
      return {
        ...state,
        isRequest: false,
        error: action.error,
        requestDone: false,
        isRefresh: false
      };
    case "CHANGE_STATUS_STAFF": {
      const { listStaff } = state;
      const newList = [];
      listStaff.forEach(v => {
        if (v.id === action.staffId) {
          newList.push({
            ...v,
            isLoading: true
          });
        } else {
          newList.push(v);
        }
      });
      return {
        ...state,
        listStaff: newList,
        isRefresh: false
      };
    }
    case "RESET_PASS_STAFF_STATUS": {
      const { listStaff } = state;
      const newList = [];
      listStaff.forEach(v => {
        if (v.id === action.staffId) {
          newList.push({
            ...v,
            isResetPass: true
          });
        } else {
          newList.push(v);
        }
      });
      return {
        ...state,
        listStaff: newList,
        isRefresh: false
      };
    }
    case "CLEAR_DATA_STAFF":
      return {
        isRequest: false,
        error: null,
        requestDone: false,
        listStaff: null,
        totalStaff: 0,
        isRefresh: false
      };
    default:
      return state;
  }
};

export default StaffManagerReducer;
