const AmountReducer = (
  state = {
    isRequest: false,
    isRequestListBenefit: false,
    error: null,
    requestDone: false,
    listBenefit: null,
    isRefresh: false
  },
  action
) => {
  switch (action.type) {
    case "REQUEST_AMOUNT_BENEFIT":
      return {
        ...state,
        isRequestListBenefit: true,
        error: null,
        requestDone: false,
        isRefresh: false
      };
    case "REQUEST_AMOUNT_BENEFIT_DONE":
      return {
        ...state,
        isRequestListBenefit: false,
        error: null,
        requestDone: true,
        listBenefit: action.listUser.benefits,
        isRefresh: false
      };
    case "REQUEST_AMOUNT_BENEFIT_FAIL":
      return {
        ...state,
        isRequestListBenefit: false,
        error: action.error,
        requestDone: false,
        listBenefit: [],
        isRefresh: false
      };
    case "CLEAR_DATA_AMOUNT_BENEFIT":
      return {
        isRequestListBenefit: false,
        isRequest: false,
        error: null,
        requestDone: false,
        listBenefit: null,
        isRefresh: false
      };
    default:
      return state;
  }
};

export default AmountReducer;
