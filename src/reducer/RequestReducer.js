const RequestReducer = (
  state = {
    isRequest: false,
    error: null,
    requestDone: false,
    isRefresh: false,
    data: null
  },
  action
) => {
  switch (action.type) {
    case "REQUEST_START":
      return {
        ...state,
        isRequest: true,
        error: null,
        requestDone: false,
        isRefresh: false,
        data: null
      };
    case "REQUEST_DONE":
      return {
        ...state,
        isRequest: false,
        isRefresh: action.isRefresh ? action.isRefresh : false,
        error: null,
        requestDone: true,
        data: action.data
      };
    case "REQUEST_FAIL": {
      let { error } = action;

      if (error === "old.pass.incorrect") {
        error = "Mật khẩu cũ không chính xác";
      } else if (error === "user.notfound") {
        error = "Toàn khoản không tồn tại";
      } else if (error === "failed") {
        error = "Lỗi hệ thống";
      }

      return {
        ...state,
        isRequest: false,
        error,
        requestDone: false,
        isRefresh: false,
        data: null
      };
    }
    case "REQUEST_ERROR": {
      const { error } = action;
      return {
        ...state,
        isRequest: false,
        error,
        requestDone: false,
        isRefresh: false
      };
    }
    case "REQUEST_REFRESH":
      return {
        ...state,
        isRequest: false,
        error: null,
        requestDone: false,
        data: null,
        isRefresh: false
      };
    default:
      return state;
  }
};

export default RequestReducer;
