const TagSkillReducer = (
  state = {
    isRequest: false,
    error: null,
    requestDone: false,
    listTagSkill: null,
    totalSkill: 0
  },
  action
) => {
  switch (action.type) {
    case "REQUEST_LIST_SKILL":
      return {
        ...state,
        isRequest: true,
        error: null,
        requestDone: false
      };
    case "REQUEST_LIST_SKILL_DONE":
      return {
        ...state,
        isRequest: false,
        error: null,
        requestDone: true,
        listTagSkill: action.listTagSkill.skills,
        totalSkill: action.listTagSkill.total
      };
    case "REQUEST_LIST_SKILL_FAIL":
      return {
        ...state,
        isRequest: false,
        error: action.error,
        requestDone: false,
        listTagSkill: [],
        totalSkill: 0
      };
    case "CHANGE_STATUS_SKILL": {
      const { listTagSkill } = state;
      const newList = [];
      listTagSkill.forEach(v => {
        if (v.id === action.skillId) {
          newList.push({
            ...v,
            isLoading: true
          });
        } else {
          newList.push(v);
        }
      });
      return {
        ...state,
        listTagSkill: newList
      };
    }
    case "CLEAR_DATA_SKILL":
      return {
        isRequest: false,
        error: null,
        requestDone: false,
        listTagSkill: null,
        totalSkill: 0
      };
    default:
      return state;
  }
};

export default TagSkillReducer;
