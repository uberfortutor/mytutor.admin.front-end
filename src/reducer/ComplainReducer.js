const ComplainReducer = (
  state = {
    isRequest: false,
    error: null,
    requestDone: false,
    listComplain: null,
    totalComplain: 0
  },
  action
) => {
  switch (action.type) {
    case "REQUEST_LIST_COMPLAIN":
      return {
        ...state,
        isRequest: true,
        error: null,
        requestDone: false
      };
    case "REQUEST_LIST_COMPLAIN_DONE":
      return {
        ...state,
        isRequest: false,
        error: null,
        requestDone: true,
        listComplain: action.listComplain.complains,
        totalComplain: action.listComplain.total
      };
    case "REQUEST_LIST_COMPLAIN_FAIL":
      return {
        ...state,
        isRequest: false,
        error: action.error,
        requestDone: false,
        listComplain: [],
        totalComplain: 0
      };
    case "CHANGE_STATUS_COMPLAIN": {
      const { listComplain } = state;
      const newList = [];
      listComplain.forEach(v => {
        if (v.id === action.complainId) {
          newList.push({
            ...v,
            isLoading: true
          });
        } else {
          newList.push(v);
        }
      });
      return {
        ...state,
        listComplain: newList
      };
    }
    case "CLEAR_COMPLAIN":
      return {
        isRequest: false,
        error: null,
        requestDone: false,
        listComplain: null,
        totalComplain: 0
      };
    default:
      return state;
  }
};

export default ComplainReducer;
