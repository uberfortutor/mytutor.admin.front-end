const TopUserReducer = (
  state = {
    isRequest: false,
    error: null,
    requestDone: false,
    listUser: null,
    totalUser: 0
  },
  action
) => {
  switch (action.type) {
    case "REQUEST_TOP_LIST":
      return {
        ...state,
        isRequest: true,
        error: null,
        requestDone: false,
        totalUser: 0
      };
    case "REQUEST_TOP_LIST_DONE":
      return {
        ...state,
        isRequest: false,
        error: null,
        requestDone: true,
        listUser: action.listUser.tops,
        totalUser: action.listUser.total
      };
    case "REQUEST_TOP_LIST_FAIL":
      return {
        ...state,
        isRequest: false,
        error: action.error,
        requestDone: false,
        listUser: [],
        totalUser: 0
      };
    case "CLEAR_DATA_TOP_USER":
      return {
        isRequest: false,
        error: null,
        requestDone: false,
        listUser: null,
        totalUser: 0
      };
    default:
      return state;
  }
};

export default TopUserReducer;
