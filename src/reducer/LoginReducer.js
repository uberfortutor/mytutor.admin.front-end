const LoginReducer = (
  state = {
    isRequest: false,
    error: null,
    requestDone: false
  },
  action
) => {
  switch (action.type) {
    case "REQUEST_LOGIN":
      return {
        ...state,
        isRequest: true,
        error: null,
        requestDone: false
      };
    case "REQUEST_LOGIN_DONE":
      localStorage.setItem("userToken", action.tokenInfo.token);
      return {
        ...state,
        isRequest: false,
        error: null,
        requestDone: true
      };
    case "REQUEST_LOGIN_FAIL":
      return {
        ...state,
        isRequest: false,
        error: action.error,
        requestDone: false
      };
    case "CLEAR_DATA_LOGIN":
      return {
        isRequest: false,
        error: null,
        requestDone: false
      };
    default:
      return state;
  }
};

export default LoginReducer;
