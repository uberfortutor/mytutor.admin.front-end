const HomeReducer = (
  state = {
    isRequest: false,
    error: null,
    requestDone: false,
    isRefresh: false,
    role: undefined
  },
  action
) => {
  switch (action.type) {
    case "REQUEST_PING_START":
      return {
        ...state,
        isRequest: true,
        error: null,
        requestDone: false,
        isRefresh: false,
        role: undefined
      };
    case "REQUEST_PING_DONE":
      return {
        ...state,
        isRequest: false,
        isRefresh: action.isRefresh ? action.isRefresh : false,
        error: null,
        requestDone: true,
        role: action.role
      };
    case "REQUEST_PING_FAIL":
      return {
        ...state,
        isRequest: false,
        error: action.error,
        requestDone: false,
        isRefresh: false,
        role: undefined
      };
    case "REQUEST_PING_REFRESH":
      return {
        ...state,
        isRequest: false,
        error: null,
        requestDone: false,
        isRefresh: false,
        role: undefined
      };
    default:
      return state;
  }
};

export default HomeReducer;
