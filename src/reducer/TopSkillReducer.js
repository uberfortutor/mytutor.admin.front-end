const TopSkillReducer = (
  state = {
    isRequest: false,
    error: null,
    requestDone: false,
    listSkill: null,
    totalSkill: 0
  },
  action
) => {
  switch (action.type) {
    case "REQUEST_TOP_SKILL_LIST":
      return {
        ...state,
        isRequest: true,
        error: null,
        requestDone: false,
        totalSkill: 0
      };
    case "REQUEST_TOP_SKILL_LIST_DONE":
      return {
        ...state,
        isRequest: false,
        error: null,
        requestDone: true,
        listSkill: action.listSkill.tops,
        totalSkill: action.listSkill.total
      };
    case "REQUEST_TOP_SKILL_LIST_FAIL":
      return {
        ...state,
        isRequest: false,
        error: action.error,
        requestDone: false,
        listSkill: [],
        totalSkill: 0
      };
    case "CLEAR_DATA_TOP_SKILL":
      return {
        isRequest: false,
        error: null,
        requestDone: false,
        listSkill: null,
        totalSkill: 0
      };
    default:
      return state;
  }
};

export default TopSkillReducer;
